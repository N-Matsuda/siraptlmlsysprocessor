﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;


namespace SiraPtlMlSysProcessor
{
    class ComExecClass
    {
        protected int cmpno { get; set; }
        protected string cmpname { get; set; } //会社名 English
        protected string jpcmpname { get; set; } //会社名 日本語
        public bool IsWorking { get; set; }
        protected DataUnitTable dttbl;  //施設情報
        LeveCompanyProfile lvcprof;     //会社情報
        protected string skkcode;
        protected const int MAXDATAOUT = 180; //データ無し判定の最大分数
        public ComExecClass(string cpname, string jpname, int cpno)
        {
            IsWorking = true;
            cmpname = cpname;
            jpcmpname = jpname;
            cmpno = cpno;
        }

        //----------- データ無しリスト ------------------------------------------
        protected struct NoDataSt
        {
            public string skkcode;
            public int nodatamin;
        }
        protected List<NoDataSt> nodataon;
        protected List<NoDataSt> nodataoff;

        //----------- 減警報リスト------------------------------------------
        protected struct LowDataSt
        {
            public string skkcode;
            public List<int> tnklst;
            public List<int> oiltype;
        }
        protected List<LowDataSt> lowdataon;
        protected List<LowDataSt> lowdataoff;

        //----------- 満警報リスト------------------------------------------
        protected struct HighDataSt
        {
            public string skkcode;
            public List<int> tnklst;
            public List<int> oiltype;
        }
        protected List<HighDataSt> highdataon;
        protected List<HighDataSt> highdataoff;

        //----------- 水警報リスト------------------------------------------
        protected struct WtrDataSt
        {
            public string skkcode;
            public List<int> tnklst;
            public List<int> oiltype;
            public List<string> wtrlvl;
            public List<string> wtrvol;
        }
        protected List<WtrDataSt> wtrdataon;
        protected List<WtrDataSt> wtrdataoff;

        //----------- センサー異常警報リスト ------------------------------------------
        protected struct SnsDataSt
        {
            public string skkcode;
            public List<int> tnklst;
            public List<int> oiltype;
        }
        protected List<SnsDataSt> snsdataon;
        protected List<SnsDataSt> snsdataoff;

        //----------- 漏えい点検警報リスト ------------------------------------------
        protected struct Lc4DataSt
        {
            public string skkcode;
            public List<int> tnklst;
            public List<int> oiltype;
            public List<string> rslt;
        }
        protected List<Lc4DataSt> lc4dataon;
        protected List<Lc4DataSt> lc4dataoff;

        //----------- リーク警報リスト ------------------------------------------
        protected struct LkDataSt
        {
            public string skkcode;
            public List<int> oiltype;
            public List<int> tnklst;
        }
        protected List<LkDataSt> lkdataon;
        protected List<LkDataSt> lkdataoff;

        protected List<ErrDataDetail> erdetaillst; //警報発生詳細リスト

        //通信スレッドメインルーチン
        public void ComThread_Main()
        {
            int numsite;
            LogData.WriteLog(cmpname + " 処理開始");
            lvcprof = new LeveCompanyProfile();                 //会社名に対応する会社情報を開き会社番号を取り出す
            lvcprof.OpenTable(cmpname);

            dttbl = new DataUnitTable();
            dttbl.OpenTableSkkcodesGwMbl(lvcprof.gwdefcode, lvcprof.mrdefcode, "*", lvcprof.compname2);
            numsite = dttbl.GetNumOfRecord();

            if (numsite == 0)
            {
                IsWorking = false;
                return;
            }
            DateTime dtnow = DateTime.Now.ToLocalTime();

            nodataon = new List<NoDataSt>();
            nodataoff = new List<NoDataSt>();
            lowdataon = new List<LowDataSt>();
            lowdataoff = new List<LowDataSt>();
            highdataon = new List<HighDataSt>();
            highdataoff = new List<HighDataSt>();
            wtrdataon = new List<WtrDataSt>();
            wtrdataoff = new List<WtrDataSt>();
            snsdataon = new List<SnsDataSt>();
            snsdataoff = new List<SnsDataSt>();
            lc4dataon = new List<Lc4DataSt>();
            lc4dataoff = new List<Lc4DataSt>();
            lkdataon = new List<LkDataSt>();
            lkdataoff = new List<LkDataSt>();

            try
            {
                for (int i = 0; i < numsite; i++)
                {
                    skkcode = dttbl.GetSSCode(i);
                    ZaikoStr zs = new ZaikoStr(dttbl.GetZaikoStringLine(i));
                    zs.Analyze();       //在庫文字列分析

                    SiteInfData stinf = new SiteInfData(); //施設情報
                    stinf.OpenTableSkkcode(skkcode);
                    SiraPtlSSInf sswginf = new SiraPtlSSInf(); //Siraポータル用施設情報
                    sswginf.OpenTableSkkcode(skkcode);

                    //-------------- データ無しをチェックする -------------------------------
                    TimeSpan ts = dtnow.Subtract(zs.timestmp);
                    if ((int)ts.TotalMinutes > MAXDATAOUT)        //データ無しエラー発生
                    {
                        if (sswginf.ChkNoDataWrgSts(0, 0) == false)　//データ無し警報発生
                        {
                            NoDataSt ndat = new NoDataSt();
                            ndat.skkcode = skkcode;
                            ndat.nodatamin = (int)ts.TotalMinutes;
                            nodataon.Add(ndat);
                            sswginf.ChgNoDataWrgSts(0, 0, true, skkcode);
                        }
                        continue;                                //以降の処理を中断する
                    }
                    else if (sswginf.ChkNoDataWrgSts(0, 0) == true) //データ無しエラー解除
                    {
                        NoDataSt ndat = new NoDataSt();
                        ndat.skkcode = skkcode;
                        ndat.nodatamin = 0;
                        nodataoff.Add(ndat);
                        sswginf.ChgNoDataWrgSts(0, 0, false, skkcode);
                    }
                    sswginf.ChgTimeStamp(0, dtnow, skkcode);
                    //----------- 減警報リスト作成 ------------------------------------------
                    CheckLowWarning(zs, stinf, sswginf, skkcode);
                    //----------- 満警報リスト作成 ------------------------------------------
                    CheckHighWarning(zs, stinf, sswginf, skkcode);
                    //----------- 水警報リスト作成 ------------------------------------------
                    CheckWtrWarning(zs, stinf, sswginf, skkcode);
                    //----------- センサー異常警報リスト作成 ------------------------------------------
                    CheckSnsWarning(zs, stinf, sswginf, skkcode);
                    //----------- 漏えい点検警報リスト作成 ------------------------------------------
                    CheckLc4Warning(zs, stinf, sswginf, skkcode);
                    //----------- リーク警報リスト作成 ------------------------------------------
                    CheckLkWarning(zs, stinf, sswginf, skkcode);
                }

                //ログDB登録処理
                WriteWrgLog();
                //メール送信処理
                SendWrgMail();
                //Push通知
                SendPushNotice();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            IsWorking = false;
        }

        //減警報警報リスト作成 -------------------------------------------------------------
        private void CheckLowWarning(ZaikoStr zs, SiteInfData stinf, SiraPtlSSInf sswginf, string skkcode)
        {
            try
            {
                int numtank = zs.numtank;
                int iNumTank = GlobalVar.NUMTANK;
                string[] vollstar = zs.vollst.ToArray();
                string[] lowvolar = stinf.GetLowWarning();
                string[] oilnoar = zs.lqtypelst.ToArray();
                List<int> ollst = new List<int>();
                List<int> tnkno = new List<int>();
                List<int> lwgonlst = new List<int>();
                List<int> lwgofflst = new List<int>();

                for (int i = 0; i < numtank; i++)       //最大タンク数分ループ
                {
                    int tnkvol = 0;
                    int.TryParse(vollstar[i], out tnkvol); //在庫量
                    int lowvol = 0;
                    //廃油タンクがSiteInfDataに登録されていないことの対策
                    if( i < lowvolar.Length )
                        int.TryParse(lowvolar[i], out lowvol);
                    if ((tnkvol <= lowvol) && (lowvol > 0))
                    {
                        if (sswginf.ChkLowWrgSts(0, i) == false)　//減警報発生
                        {
                            sswginf.ChgLowWrgSts(0, i, true, skkcode);    //DBを減警報ありに更新
                            lwgonlst.Add(i + 1);
                            ollst.Add(int.Parse(oilnoar[i]));
                        }
                    }
                    else
                    {
                        if (sswginf.ChkLowWrgSts(0, i) == true)　//減警報解除
                        {
                            sswginf.ChgLowWrgSts(0, i, false, skkcode);   //DBを減警報なしに更新
                            lwgofflst.Add(i + 1);
                            ollst.Add(int.Parse(oilnoar[i]));
                        }
                    }
                }

                if (lwgonlst.Count > 0) //減警報発生タンクあり
                {
                    LowDataSt lowdat = new LowDataSt();
                    lowdat.skkcode = skkcode;
                    lowdat.tnklst = lwgonlst;
                    lowdat.oiltype = ollst;
                    lowdataon.Add(lowdat);
                }
                if (lwgofflst.Count > 0) //減警報解除タンクあり
                {
                    LowDataSt lowdat = new LowDataSt();
                    lowdat.skkcode = skkcode;
                    lowdat.tnklst = lwgofflst;
                    lowdat.oiltype = ollst;
                    lowdataoff.Add(lowdat);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(skkcode + " " + ex.ToString());
            }
            Console.WriteLine(skkcode + "End check low warning\r\n");
        }

        //----------- 満警報リスト作成 ------------------------------------------
        private void CheckHighWarning(ZaikoStr zs, SiteInfData stinf, SiraPtlSSInf sswginf, string skkcode)
        {
            try
            {
                int numtank = zs.numtank;
                int iNumTank = GlobalVar.NUMTANK;
                string[] statar = zs.sslanstat.ToArray();
                string[] oilnoar = zs.lqtypelst.ToArray();
                List<int> ollst = new List<int>();
                List<int> tnkno = new List<int>();
                List<int> highonlst = new List<int>();
                List<int> highofflst = new List<int>();

                for (int i = 0; i < numtank; i++)       //最大タンク数分ループ
                {
                    //SSLANステータスの満警報をチェック
                    if ((statar[i] == "1") || (statar[i] == "4"))
                    {
                        if (sswginf.ChkHighWrgSts(0, i) == false)　//満警報発生
                        {
                            sswginf.ChgHighWrgSts(0, i, true, skkcode);    //DBを満警報ありに更新
                            highonlst.Add(i + 1);
                            ollst.Add(int.Parse(oilnoar[i]));
                        }
                    }
                    else
                    {
                        if (sswginf.ChkHighWrgSts(0, i) == true)　//満警報解除
                        {
                            sswginf.ChgHighWrgSts(0, i, false, skkcode);   //DBを満警報なしに更新
                            highofflst.Add(i + 1);
                            ollst.Add(int.Parse(oilnoar[i]));
                        }
                    }
                }

                if (highonlst.Count > 0)    //満警報発生タンクあり
                {
                    HighDataSt hidat = new HighDataSt();
                    hidat.skkcode = skkcode;
                    hidat.tnklst = highonlst;
                    hidat.oiltype = ollst;
                    highdataon.Add(hidat);
                }
                if (highofflst.Count > 0)    //満警報解除タンクあり
                {
                    HighDataSt hidat = new HighDataSt();
                    hidat.skkcode = skkcode;
                    hidat.tnklst = highofflst;
                    hidat.oiltype = ollst;
                    highdataoff.Add(hidat);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Console.WriteLine(skkcode + "End check high warning\r\n");
        }

        //----------- 水警報リスト作成------------------------------------------
        private void CheckWtrWarning(ZaikoStr zs, SiteInfData stinf, SiraPtlSSInf sswginf, string skkcode)
        {
            try
            {
                int numtank = zs.numtank;
                int iNumTank = GlobalVar.NUMTANK;
                string[] statar = zs.sslanstat.ToArray();
                string[] wtrlvl = zs.wtrlvllst.ToArray();
                string[] wtrvol = zs.wtrvollst.ToArray();

                string[] oilnoar = zs.lqtypelst.ToArray();
                List<int> ollst = new List<int>();
                List<int> tnkno = new List<int>();
                List<int> wtronlst = new List<int>();
                List<int> wtrofflst = new List<int>();
                List<string> wtrlvllst = new List<string>();
                List<string> wtrvollst = new List<string>();

                for (int i = 0; i < numtank; i++)       //最大タンク数分ループ
                {
                    //SSLANステータスの水警報をチェック
                    if ((statar[i] == "4") || (statar[i] == "5"))
                    {
                        if (sswginf.ChkWtrWrgSts(0, i) == false)　//水警報発生
                        {
                            sswginf.ChgWtrWrgSts(0, i, true, skkcode);    //DBを水警報ありに更新
                            wtronlst.Add(i + 1);
                            ollst.Add(int.Parse(oilnoar[i]));
                            wtrlvllst.Add(wtrlvl[i]);
                            wtrvollst.Add(wtrvol[i]);
                        }
                    }
                    else
                    {
                        if (sswginf.ChkWtrWrgSts(0, i) == true)　//水警報解除
                        {
                            sswginf.ChgWtrWrgSts(0, i, false, skkcode);   //DBを水警報なしに更新
                            wtrofflst.Add(i + 1);
                            ollst.Add(int.Parse(oilnoar[i]));
                        }
                    }
                }

                if (wtronlst.Count > 0)     //水警報発生タンクあり
                {
                    WtrDataSt wtdat = new WtrDataSt();
                    wtdat.skkcode = skkcode;
                    wtdat.tnklst = wtronlst;
                    wtdat.oiltype = ollst;
                    wtdat.wtrlvl = wtrlvllst;
                    wtdat.wtrvol = wtrvollst;
                    wtrdataon.Add(wtdat);
                }
                if (wtrofflst.Count > 0)    //水警報解除タンクあり
                {
                    WtrDataSt wtdat = new WtrDataSt();
                    wtdat.skkcode = skkcode;
                    wtdat.tnklst = wtrofflst;
                    wtdat.oiltype = ollst;
                    wtrdataoff.Add(wtdat);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Console.WriteLine(skkcode + "End check water warning\r\n");
        }
        //----------- センサー異常警報リスト作成 ------------------------------------------
        private void CheckSnsWarning(ZaikoStr zs, SiteInfData stinf, SiraPtlSSInf sswginf, string skkcode)
        {
            try
            {
                int numtank = zs.numtank;
                int iNumTank = GlobalVar.NUMTANK;
                string[] statar = zs.sslan2stat.ToArray();
                byte[] stsbyte;

                string[] oilnoar = zs.lqtypelst.ToArray();
                List<int> ollst = new List<int>();
                List<int> tnkno = new List<int>();
                List<int> snsonlst = new List<int>();
                List<int> snsofflst = new List<int>();

                for (int i = 0; i < numtank; i++)       //最大タンク数分ループ
                {
                    //SSLANステータスのセンサー警報をチェック
                    stsbyte = Encoding.ASCII.GetBytes(statar[i]);
                    if ((stsbyte[0] & 0x02) == 0x02)
                    {
                        if (sswginf.ChkSnsWrgSts(0, i) == false)　//センサー警報発生
                        {
                            sswginf.ChgSnsWrgSts(0, i, true, skkcode);    //DBをセンサー警報ありに更新
                            snsonlst.Add(i + 1);
                            ollst.Add(int.Parse(oilnoar[i]));
                        }
                    }
                    else
                    {
                        if (sswginf.ChkSnsWrgSts(0, i) == true)　//センサー警報解除
                        {
                            sswginf.ChgSnsWrgSts(0, i, false, skkcode);   //DBをセンサー警報なしに更新
                            snsofflst.Add(i + 1);
                            ollst.Add(int.Parse(oilnoar[i]));
                        }
                    }
                }

                if (snsonlst.Count > 0) //センサー警報発生タンクあり
                {
                    SnsDataSt sndat = new SnsDataSt();
                    sndat.skkcode = skkcode;
                    sndat.tnklst = snsonlst;
                    sndat.oiltype = ollst;
                    snsdataon.Add(sndat);
                }
                if (snsofflst.Count > 0)    //センサー警報解除タンクあり
                {
                    SnsDataSt sndat = new SnsDataSt();
                    sndat.skkcode = skkcode;
                    sndat.tnklst = snsofflst;
                    sndat.oiltype = ollst;
                    snsdataoff.Add(sndat);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Console.WriteLine(skkcode + "End check abnormal senser warning\r\n");
        }
        //----------- 漏えい点検警報リスト作成 ------------------------------------------
        private void CheckLc4Warning(ZaikoStr zs, SiteInfData stinf, SiraPtlSSInf sswginf, string skkcode)
        {
            try
            {
                int numtank = zs.numtank;
                int iNumTank = GlobalVar.NUMTANK;
                string[] statar = zs.sslan2stat.ToArray();
                byte[] stsbyte;
                string[] lc4rsar = zs.lc4inflst.ToArray();

                string[] oilnoar = zs.lqtypelst.ToArray();
                List<int> ollst = new List<int>();
                List<int> tnkno = new List<int>();
                List<int> lc4onlst = new List<int>();
                List<int> lc4offlst = new List<int>();
                List<string> lc4rst = new List<string>();

                for (int i = 0; i < numtank; i++)       //最大タンク数分ループ
                {
                    //SSLANステータスの漏えい警報をチェック
                    stsbyte = Encoding.ASCII.GetBytes(statar[i]);
                    if ((stsbyte[1] & 0x0c) > 0)
                    {
                        if (sswginf.ChkLc4WrgSts(0, i) == false)　//漏えい警報発生
                        {
                            sswginf.ChgLc4WrgSts(0, i, true, skkcode);    //DBを漏えい警報ありに更新
                            lc4onlst.Add(i + 1);
                            ollst.Add(int.Parse(oilnoar[i]));
                            lc4rst.Add(lc4rsar[i]);
                        }
                    }
                    else
                    {
                        if (sswginf.ChkLc4WrgSts(0, i) == true)　//漏えい警報解除
                        {
                            sswginf.ChgLc4WrgSts(0, i, false, skkcode);   //DBを漏えい警報なしに更新
                            lc4offlst.Add(i + 1);
                            ollst.Add(int.Parse(oilnoar[i]));
                        }
                    }
                }

                if (lc4onlst.Count > 0) //漏えい警報発生タンクあり
                {
                    Lc4DataSt lcdat = new Lc4DataSt();
                    lcdat.skkcode = skkcode;
                    lcdat.tnklst = lc4onlst;
                    lcdat.oiltype = ollst;
                    lcdat.rslt = lc4rst;
                    lc4dataon.Add(lcdat);
                }
                if (lc4offlst.Count > 0)    //漏えい警報解除タンクあり
                {
                    Lc4DataSt lcdat = new Lc4DataSt();
                    lcdat.skkcode = skkcode;
                    lcdat.tnklst = lc4offlst;
                    lcdat.oiltype = ollst;
                    lc4dataoff.Add(lcdat);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Console.WriteLine(skkcode + "End check leak test error warning\r\n");
        }

        //----------- リーク警報リスト作成 ------------------------------------------
        private void CheckLkWarning(ZaikoStr zs, SiteInfData stinf, SiraPtlSSInf sswginf, string skkcode)
        {
            try
            {
                int numtank = zs.numtank;
                int iNumTank = GlobalVar.NUMTANK;
                //string[] OilType = stinf.GetOilType();
                string[] statar = zs.sslan2stat.ToArray();
                byte[] stsbyte;

                string[] oilnoar = zs.lqtypelst.ToArray();
                List<int> ollst = new List<int>();
                List<int> tnkno = new List<int>();
                List<int> lkonlst = new List<int>();
                List<int> lkofflst = new List<int>();

                for (int i = 0; i < numtank; i++)       //最大タンク数分ループ
                {
                    //SSLANステータスのリーク警報をチェック
                    stsbyte = Encoding.ASCII.GetBytes(statar[i]);
                    if ((stsbyte[0] & 0x01) > 0)
                    {
                        if (sswginf.ChkLkWrgSts(0, i) == false)　//リーク警報発生
                        {
                            sswginf.ChgLkWrgSts(0, i, true, skkcode);    //DBをリーク警報ありに更新
                            lkonlst.Add(i + 1);
                            ollst.Add(int.Parse(oilnoar[i]));
                        }
                    }
                    else
                    {
                        if (sswginf.ChkLkWrgSts(0, i) == true)　//リーク警報解除
                        {
                            sswginf.ChgLkWrgSts(0, i, false, skkcode);   //DBをリーク警報なしに更新
                            lkofflst.Add(i + 1);
                            ollst.Add(int.Parse(oilnoar[i]));
                        }
                    }
                }

                if (lkonlst.Count > 0)  //リーク警報発生タンクあり
                {
                    LkDataSt lkdat = new LkDataSt();
                    lkdat.skkcode = skkcode;
                    lkdat.tnklst = lkonlst;
                    lkdat.oiltype = ollst;
                    lkdataon.Add(lkdat);
                }
                if (lkofflst.Count > 0) //リーク警報解除タンクあり
                {
                    LkDataSt lkdat = new LkDataSt();
                    lkdat.skkcode = skkcode;
                    lkdat.tnklst = lkofflst;
                    lkdat.oiltype = ollst;
                    lkdataoff.Add(lkdat);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Console.WriteLine(skkcode + "End check leak error error warning\r\n");
        }


        //ログDB登録処理
        protected void WriteWrgLog()
        {
            string skkcode;
            DateTime dtnow = DateTime.Now.ToLocalTime();
            string datestr = dtnow.ToString("yyyyMMddHHmm");
            erdetaillst = new List<ErrDataDetail>();
            SiraPtlSSInf ssinf = new SiraPtlSSInf();
            ssinf.OpenTable(jpcmpname);
            try
            {
                //----------- データ無しON リスト ------------------------------------------
                foreach (var nodat in nodataon)
                {
                    skkcode = nodat.skkcode;
                    string sitename = ssinf.GetSiteName(skkcode);
                    ErrDataDetail edat = new ErrDataDetail(sitename, skkcode, jpcmpname, 0, 0, datestr, (int)WrgType.NoDataWrg, 1, 0, 0, "");
                    erdetaillst.Add(edat);
                    SiraPtlWrgHis.WriteWrgHistory(edat);
                    Console.WriteLine("No Data Wrg On" + skkcode);
                }
                //----------- データ無しOFF リスト ------------------------------------------
                foreach (var nodat in nodataoff)
                {
                    skkcode = nodat.skkcode;
                    string sitename = ssinf.GetSiteName(skkcode);
                    ErrDataDetail edat = new ErrDataDetail(sitename, skkcode, jpcmpname, 0, 0, datestr, (int)WrgType.NoDataWrg, 0, 0, 0, "");
                    SiraPtlWrgHis.WriteWrgHistory(edat);
                    Console.WriteLine("No Data Wrg Off" + skkcode);
                }
                //----------- 減警報ONリスト------------------------------------------
                foreach (var lodat in lowdataon)
                {
                    skkcode = lodat.skkcode;
                    string sitename = ssinf.GetSiteName(skkcode);
                    int[] tankar = lodat.tnklst.ToArray();
                    int[] oilar = lodat.oiltype.ToArray();
                    int numtnk = tankar.Count();
                    for (int i = 0; i < numtnk; i++)
                    {
                        ErrDataDetail edat = new ErrDataDetail(sitename, skkcode, jpcmpname, oilar[i], tankar[i], datestr, (int)WrgType.LowWrg, 1, 0, 0, "");
                        erdetaillst.Add(edat);
                        SiraPtlWrgHis.WriteWrgHistory(edat);
                        Console.WriteLine("Low Wrg On" + skkcode + " tank " + i.ToString());
                    }
                }
                //----------- 減警報OFFリスト------------------------------------------
                foreach (var lodat in lowdataoff)
                {
                    skkcode = lodat.skkcode;
                    string sitename = ssinf.GetSiteName(skkcode);
                    int[] tankar = lodat.tnklst.ToArray();
                    int[] oilar = lodat.oiltype.ToArray();
                    int numtnk = tankar.Count();
                    for (int i = 0; i < numtnk; i++)
                    {
                        ErrDataDetail edat = new ErrDataDetail(sitename, skkcode, jpcmpname, oilar[i], tankar[i], datestr, (int)WrgType.LowWrg, 0, 0, 0, "");
                        SiraPtlWrgHis.WriteWrgHistory(edat);
                        Console.WriteLine("Low Wrg Off" + skkcode + " tank " + i.ToString());
                    }
                }
                //----------- 満警報ONリスト------------------------------------------
                foreach (var hidat in highdataon)
                {
                    skkcode = hidat.skkcode;
                    string sitename = ssinf.GetSiteName(skkcode);
                    int[] tankar = hidat.tnklst.ToArray();
                    int[] oilar = hidat.oiltype.ToArray();
                    int numtnk = tankar.Count();
                    for (int i = 0; i < numtnk; i++)
                    {
                        ErrDataDetail edat = new ErrDataDetail(sitename, skkcode, jpcmpname, oilar[i], tankar[i], datestr, (int)WrgType.HighWrg, 1, 0, 0, "");
                        SiraPtlWrgHis.WriteWrgHistory(edat);
                        erdetaillst.Add(edat);
                        Console.WriteLine("High Wrg On" + skkcode + " tank " + i.ToString());
                    }
                }
                //----------- 満警報OFFリスト------------------------------------------
                foreach (var hidat in highdataoff)
                {
                    skkcode = hidat.skkcode;
                    string sitename = ssinf.GetSiteName(skkcode);
                    int[] tankar = hidat.tnklst.ToArray();
                    int[] oilar = hidat.oiltype.ToArray();
                    int numtnk = tankar.Count();
                    for (int i = 0; i < numtnk; i++)
                    {
                        ErrDataDetail edat = new ErrDataDetail(sitename, skkcode, jpcmpname, oilar[i], tankar[i], datestr, (int)WrgType.HighWrg, 0, 0, 0, "");
                        SiraPtlWrgHis.WriteWrgHistory(edat);
                        Console.WriteLine("High Wrg Off" + skkcode + " tank " + i.ToString());
                    }
                }
                //----------- 水警報ONリスト------------------------------------------
                foreach (var wtdat in wtrdataon)
                {
                    skkcode = wtdat.skkcode;
                    string sitename = ssinf.GetSiteName(skkcode);
                    int[] tankar = wtdat.tnklst.ToArray();
                    int[] oilar = wtdat.oiltype.ToArray();
                    int numtnk = tankar.Count();
                    string[] lvlar = wtdat.wtrlvl.ToArray();
                    string[] volar = wtdat.wtrvol.ToArray();
                    for (int i = 0; i < numtnk; i++)
                    {
                        //水位,水量 .001L単位 -> L単位変換
                        int wtrlvl = int.Parse(lvlar[i]);
                        wtrlvl = (wtrlvl + 100) / 100;
                        int wtrvol = int.Parse(volar[i]);
                        wtrvol = (wtrvol + 100) / 100;
                        ErrDataDetail edat = new ErrDataDetail(sitename, skkcode, jpcmpname, oilar[i], tankar[i], datestr, (int)WrgType.WtrWrg, 1, wtrlvl, wtrvol, "");
                        SiraPtlWrgHis.WriteWrgHistory(edat);
                        erdetaillst.Add(edat);
                        Console.WriteLine("Wtr Wrg On" + skkcode + " tank " + i.ToString());
                    }
                }
                //----------- 水警報OFFリスト------------------------------------------
                foreach (var wtdat in wtrdataoff)
                {
                    skkcode = wtdat.skkcode;
                    string sitename = ssinf.GetSiteName(skkcode);
                    int[] tankar = wtdat.tnklst.ToArray();
                    int[] oilar = wtdat.oiltype.ToArray();
                    int numtnk = tankar.Count();
                    for (int i = 0; i < numtnk; i++)
                    {
                        ErrDataDetail edat = new ErrDataDetail(sitename, skkcode, jpcmpname, oilar[i], tankar[i], datestr, (int)WrgType.WtrWrg, 0, 0, 0, "");
                        SiraPtlWrgHis.WriteWrgHistory(edat);
                        Console.WriteLine("Wtr Wrg Off" + skkcode + " tank " + i.ToString());
                    }
                }
                //----------- センサー異常警報ONリスト ------------------------------------------
                foreach (var sndat in snsdataon)
                {
                    skkcode = sndat.skkcode;
                    string sitename = ssinf.GetSiteName(skkcode);
                    int[] tankar = sndat.tnklst.ToArray();
                    int[] oilar = sndat.oiltype.ToArray();
                    int numtnk = tankar.Count();
                    for (int i = 0; i < numtnk; i++)
                    {
                        ErrDataDetail edat = new ErrDataDetail(sitename, skkcode, jpcmpname, oilar[i], tankar[i], datestr, (int)WrgType.SnsErrWrg, 1, 0, 0, "");
                        SiraPtlWrgHis.WriteWrgHistory(edat);
                        erdetaillst.Add(edat);
                        Console.WriteLine("Abnormal Sns On " + skkcode + " tank " + i.ToString());
                    }
                }
                //----------- センサー異常警報OFFリスト ------------------------------------------
                foreach (var sndat in snsdataoff)
                {
                    skkcode = sndat.skkcode;
                    string sitename = ssinf.GetSiteName(skkcode);
                    int[] tankar = sndat.tnklst.ToArray();
                    int[] oilar = sndat.oiltype.ToArray();
                    int numtnk = tankar.Count();
                    for (int i = 0; i < numtnk; i++)
                    {
                        ErrDataDetail edat = new ErrDataDetail(sitename, skkcode, jpcmpname, oilar[i], tankar[i], datestr, (int)WrgType.SnsErrWrg, 0, 0, 0, "");
                        SiraPtlWrgHis.WriteWrgHistory(edat);
                        Console.WriteLine("Abnormal Sns Off " + skkcode + " tank " + i.ToString());
                    }
                }
                //----------- 漏えい点検警報ONリスト ------------------------------------------
                foreach (var lcdat in lc4dataon)
                {
                    skkcode = lcdat.skkcode;
                    string sitename = ssinf.GetSiteName(skkcode);
                    int[] tankar = lcdat.tnklst.ToArray();
                    int[] oilar = lcdat.oiltype.ToArray();
                    string[] rsltar = lcdat.rslt.ToArray();
                    int numtnk = tankar.Count();
                    for (int i = 0; i < numtnk; i++)
                    {
                        ErrDataDetail edat = new ErrDataDetail(sitename, skkcode, jpcmpname, oilar[i], tankar[i], datestr, (int)WrgType.LC4Wrg, 1, 0, 0, rsltar[i]);
                        SiraPtlWrgHis.WriteWrgHistory(edat);
                        erdetaillst.Add(edat);
                        Console.WriteLine("LC4 error on " + skkcode + " tank " + i.ToString());
                    }
                }
                //----------- 漏えい点検警報OFFリスト ------------------------------------------
                foreach (var lcdat in lc4dataoff)
                {
                    skkcode = lcdat.skkcode;
                    string sitename = ssinf.GetSiteName(skkcode);
                    int[] tankar = lcdat.tnklst.ToArray();
                    int[] oilar = lcdat.oiltype.ToArray();
                    string[] rsltar = lcdat.rslt.ToArray();
                    int numtnk = tankar.Count();
                    for (int i = 0; i < numtnk; i++)
                    {
                        ErrDataDetail edat = new ErrDataDetail(sitename, skkcode, jpcmpname, oilar[i], tankar[i], datestr, (int)WrgType.LC4Wrg, 0, 0, 0, "");
                        SiraPtlWrgHis.WriteWrgHistory(edat);
                        Console.WriteLine("LC4 error off " + skkcode + " tank " + i.ToString());
                    }
                }
                //----------- リーク警報ONリスト ------------------------------------------
                foreach (var lkdat in lkdataon)
                {
                    skkcode = lkdat.skkcode;
                    string sitename = ssinf.GetSiteName(skkcode);
                    int[] tankar = lkdat.tnklst.ToArray();
                    int[] oilar = lkdat.oiltype.ToArray();
                    int numtnk = tankar.Count();
                    for (int i = 0; i < numtnk; i++)
                    {
                        ErrDataDetail edat = new ErrDataDetail(sitename, skkcode, jpcmpname, oilar[i], tankar[i], datestr, (int)WrgType.LeakWrg, 1, 0, 0, "");
                        SiraPtlWrgHis.WriteWrgHistory(edat);
                        erdetaillst.Add(edat);
                        Console.WriteLine("Leak error on " + skkcode + " tank " + i.ToString());
                    }
                }
                //----------- リーク警報OFFリスト ------------------------------------------
                foreach (var lkdat in lkdataoff)
                {
                    skkcode = lkdat.skkcode;
                    string sitename = ssinf.GetSiteName(skkcode);
                    int[] tankar = lkdat.tnklst.ToArray();
                    int[] oilar = lkdat.oiltype.ToArray();
                    int numtnk = tankar.Count();
                    for (int i = 0; i < numtnk; i++)
                    {
                        ErrDataDetail edat = new ErrDataDetail(sitename, skkcode, jpcmpname, oilar[i], tankar[i], datestr, (int)WrgType.LeakWrg, 0, 0, 0, "");
                        SiraPtlWrgHis.WriteWrgHistory(edat);
                        Console.WriteLine("Leak error off " + skkcode + " tank " + i.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //警報メール送信    
        protected void SendWrgMail()
        {
            try
            {
                SiraPtlUserProfile spuser = new SiraPtlUserProfile();
                spuser.OpenTableCompanyNo(cmpno);
                SiraPtlSSInf ssinf = new SiraPtlSSInf();
                int numuser = spuser.GetNumOfRecord();
                for (int i = 0; i < numuser; i++)
                {
                    string mailstr = "";
                    bool bfound = false;
                    List<string> skkcodelst = spuser.GetReferrableSkkcodeList(i);

                    foreach (string skcode in skkcodelst)
                    {
                        ssinf.OpenTableSkkcode(skcode);
                        string sitename = ssinf.GetSiteName(skcode);
                        if (spuser.ChkSendNoDataMail(i) == true) //データ無しメール送信
                        {
                            foreach (var nodat in nodataon)
                            {
                                if (skcode == nodat.skkcode)
                                {
                                    if (bfound == false)
                                    {
                                        mailstr = "施設:" + sitename + "で下記の警報発生\r\n";
                                        bfound = true;
                                    }
                                    mailstr += "一時間以上データが更新されていません\r\n";
                                }
                            }
                            foreach (var nodatof in nodataoff)
                            {
                                if (skcode == nodatof.skkcode)
                                {
                                    if (bfound == false)
                                    {
                                        mailstr = "施設:" + sitename + "で下記の警報解除\r\n";
                                        bfound = true;
                                    }
                                    mailstr += "一時間以上データ更新なし警報解除\r\n";
                                }
                            }
                        }
                        if (spuser.ChkSendLowMail(i) == true) //減メール送信
                        {
                            foreach (var lodat in lowdataon)
                            {
                                if (skcode == lodat.skkcode)
                                {
                                    if (bfound == false)
                                    {
                                        mailstr = "施設:" + sitename + "で下記の警報発生\r\n";
                                        bfound = true;
                                    }
                                    int[] tankar = lodat.tnklst.ToArray();
                                    int num = tankar.Count();
                                    for (int j = 0; j < num; j++)
                                    {
                                        mailstr += "タンク" + tankar[j].ToString() + "で減警報が発生しました\r\n";
                                    }
                                }
                            }
                        }
                        if (spuser.ChkSendHighMail(i) == true) //満メール送信
                        {
                            foreach (var hidat in highdataon)
                            {
                                if (skcode == hidat.skkcode)
                                {
                                    if (bfound == false)
                                    {
                                        mailstr = "施設:" + sitename + "で下記の警報発生\r\n";
                                        bfound = true;
                                    }
                                    int[] tankar = hidat.tnklst.ToArray();
                                    int num = tankar.Count();
                                    for (int j = 0; j < num; j++)
                                    {
                                        mailstr += "タンク" + tankar[j].ToString() + "で満警報が発生しました\r\n";
                                    }
                                }
                            }
                        }
                        if (spuser.ChkSendWtrMail(i) == true) //水メール送信
                        {
                            foreach (var wtdat in wtrdataon)
                            {
                                if (skcode == wtdat.skkcode)
                                {
                                    if (bfound == false)
                                    {
                                        mailstr = "施設:" + sitename + "で下記の警報発生\r\n";
                                        bfound = true;
                                    }
                                    int[] tankar = wtdat.tnklst.ToArray();
                                    int num = tankar.Count();
                                    for (int j = 0; j < num; j++)
                                    {
                                        mailstr += "タンク" + tankar[j].ToString() + "で水警報が発生しました\r\n";
                                    }
                                }
                            }
                        }
                        if (spuser.ChkSendSnsMail(i) == true) //センサー異常メール送信
                        {
                            foreach (var sndat in snsdataon)
                            {
                                if (skcode == sndat.skkcode)
                                {
                                    if (bfound == false)
                                    {
                                        mailstr = "施設:" + sitename + "で下記の警報発生\r\n";
                                        bfound = true;
                                    }
                                    int[] tankar = sndat.tnklst.ToArray();
                                    int num = tankar.Count();
                                    for (int j = 0; j < num; j++)
                                    {
                                        mailstr += "タンク" + tankar[j].ToString() + "でセンサー以上警報が発生しました\r\n";
                                    }
                                }
                            }
                        }
                        if (spuser.ChkSendLc4Mail(i) == true) //漏えい点検警報メール送信
                        {
                            foreach (var lcdat in lc4dataon)
                            {
                                if (skcode == lcdat.skkcode)
                                {
                                    if (bfound == false)
                                    {
                                        mailstr = "施設:" + sitename + "で下記の警報発生\r\n";
                                        bfound = true;
                                    }
                                    int[] tankar = lcdat.tnklst.ToArray();
                                    int num = tankar.Count();
                                    for (int j = 0; j < num; j++)
                                    {
                                        mailstr += "タンク" + tankar[j].ToString() + "で漏えい点検警報が発生しました\r\n";
                                    }
                                }
                            }
                        }
                        if (spuser.ChkSendLeakMail(i) == true) //リーク警報メール送信
                        {
                            foreach (var lkdat in lkdataon)
                            {
                                if (skcode == lkdat.skkcode)
                                {
                                    if (bfound == false)
                                    {
                                        mailstr = "施設:" + sitename + "で下記の警報発生\r\n";
                                        bfound = true;
                                    }
                                    int[] tankar = lkdat.tnklst.ToArray();
                                    int num = tankar.Count();
                                    for (int j = 0; j < num; j++)
                                    {
                                        mailstr += "タンク" + tankar[j].ToString() + "で漏えい警報が発生しました\r\n";
                                    }
                                }
                            }
                        }
                    }
                    if (bfound == true)
                        SendMail.SendMailExec(mailstr, spuser.GetMailAddress(i));
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        //"Rooms"へプッシュ通知
        protected void SendPushNotice()
        {
            try
            {
                string skkcode = "";
                string sitename = "";
                string emailadr = "";
                SIRASite siteinf = new SIRASite();
                ANUserProfile uprof = new ANUserProfile();
                //データ無し
                foreach (var nodat in nodataon)
                {
                    skkcode = nodat.skkcode;
                    uprof.OpenTable(skkcode);
                    int num = uprof.GetNumOfRecord();
                    if (num > 0) //対象ユーザーが居れば送信
                    {
                        try
                        {
                            for (int i = 0; i < num; i++)
                            {
                                emailadr = uprof.GetMailAdr(i);
                                siteinf.OpenTableSkkCode(skkcode);
                                sitename = siteinf.GetSiteName(0);
                                int[] tno = { 0 };
                                PushNotice.PushNoticeExec(emailadr, skkcode, sitename, (int)WrgType.NoDataWrg, tno);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }
                }
                //減警報
                foreach (var lodat in lowdataon)
                {
                    skkcode = lodat.skkcode;
                    uprof.OpenTable(skkcode);
                    int num = uprof.GetNumOfRecord();
                    if (num > 0) //対象ユーザーが居れば送信
                    {
                        try
                        {
                            for (int i = 0; i < num; i++)
                            {
                                emailadr = uprof.GetMailAdr(i);
                                siteinf.OpenTableSkkCode(skkcode);
                                sitename = siteinf.GetSiteName(0);
                                int[] tankar = lodat.tnklst.ToArray();
                                PushNotice.PushNoticeExec(emailadr, skkcode, sitename, (int)WrgType.LowWrg, tankar);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }
                }
                //満警報
                foreach (var hidat in highdataon)
                {
                    skkcode = hidat.skkcode;
                    uprof.OpenTable(skkcode);
                    int num = uprof.GetNumOfRecord();
                    if (num > 0) //対象ユーザーが居れば送信
                    {
                        try
                        {
                            for (int i = 0; i < num; i++)
                            {
                                emailadr = uprof.GetMailAdr(i);
                                siteinf.OpenTableSkkCode(skkcode);
                                sitename = siteinf.GetSiteName(0);
                                int[] tankar = hidat.tnklst.ToArray();
                                PushNotice.PushNoticeExec(emailadr, skkcode, sitename, (int)WrgType.HighWrg, tankar);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }
                }
                //水警報
                foreach (var wtdat in wtrdataon)
                {
                    skkcode = wtdat.skkcode;
                    uprof.OpenTable(skkcode);
                    int num = uprof.GetNumOfRecord();
                    if (num > 0) //対象ユーザーが居れば送信
                    {
                        try
                        {
                            for (int i = 0; i < num; i++)
                            {
                                emailadr = uprof.GetMailAdr(i);
                                siteinf.OpenTableSkkCode(skkcode);
                                sitename = siteinf.GetSiteName(0);
                                int[] tankar = wtdat.tnklst.ToArray();
                                PushNotice.PushNoticeExec(emailadr, skkcode, sitename, (int)WrgType.WtrWrg, tankar);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }
                }
                //センサー異常メール送信
                foreach (var sndat in snsdataon)
                {
                    skkcode = sndat.skkcode;
                    uprof.OpenTable(skkcode);
                    int num = uprof.GetNumOfRecord();
                    if (num > 0) //対象ユーザーが居れば送信
                    {
                        try
                        {
                            for (int i = 0; i < num; i++)
                            {
                                emailadr = uprof.GetMailAdr(i);
                                siteinf.OpenTableSkkCode(skkcode);
                                sitename = siteinf.GetSiteName(0);
                                int[] tankar = sndat.tnklst.ToArray();
                                PushNotice.PushNoticeExec(emailadr, skkcode, sitename, (int)WrgType.SnsErrWrg, tankar);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }
                }
                //漏えい点検
                foreach (var lcdat in lc4dataon)
                {
                    skkcode = lcdat.skkcode;
                    uprof.OpenTable(skkcode);
                    int num = uprof.GetNumOfRecord();
                    if (num > 0) //対象ユーザーが居れば送信
                    {
                        try
                        {
                            for (int i = 0; i < num; i++)
                            {
                                emailadr = uprof.GetMailAdr(i);
                                siteinf.OpenTableSkkCode(skkcode);
                                sitename = siteinf.GetSiteName(0);
                                int[] tankar = lcdat.tnklst.ToArray();
                                PushNotice.PushNoticeExec(emailadr, skkcode, sitename, (int)WrgType.LC4Wrg, tankar);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }
                }
                //リーク
                foreach (var lkdat in lkdataon)
                {
                    skkcode = lkdat.skkcode;
                    uprof.OpenTable(skkcode);
                    int num = uprof.GetNumOfRecord();
                    if (num > 0) //対象ユーザーが居れば送信
                    {
                        try
                        {
                            for (int i = 0; i < num; i++)
                            {
                                emailadr = uprof.GetMailAdr(i);
                                siteinf.OpenTableSkkCode(skkcode);
                                sitename = siteinf.GetSiteName(0);
                                int[] tankar = lkdat.tnklst.ToArray();
                                PushNotice.PushNoticeExec(emailadr, skkcode, sitename, (int)WrgType.LeakWrg, tankar);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
