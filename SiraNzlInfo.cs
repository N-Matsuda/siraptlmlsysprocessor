﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Diagnostics;


namespace SiraPtlMlSysProcessor
{
    class SiraNzlInfo
    {
        private DataTable SiraNozlDT;
        private const int IdColNo = 0;
        private const int SKKCodeColNo = 1;     //SKKコード
        private const int L1Nozzle = 2;        //ライン1
        private const int L2Nozzle = 3;        //ライン2
        private const int L3Nozzle = 4;        //ライン3
        private string skkcode;

        const int DATAOFFS = 6;    //1計量器当たりのデータサイズ
        public SiraNzlInfo()
        {
            DataTableCtrl.InitializeTable(SiraNozlDT);
        }
        //テーブル読み込み 会社名
        public void OpenTable(string skcode)
        {
            try
            {
                skkcode = skcode;
                string sqlstr = "SELECT * FROM SiraNzlInfo WHERE SKKコード= '" + skkcode + "'";
                DataTableCtrl.InitializeTable(SiraNozlDT);
                SiraNozlDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SiraNozlDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        //レコード数取得
        public int GetNumOfRecord()
        {
            try
            {
                return SiraNozlDT.Rows.Count;

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return 0;
            }
        }
        //指定したライン番号(lno : 1-3), 計量器番号(keyno: 1-44), ノズル番号(nzlno: 1-3)から対応するタンク番号を返す。
        public int GetNzlTankNo(int lno, int keyno, int nzlno)
        {
            int tankno = 0;
            try
            {
                if (SiraNozlDT != null)
                {
                    string keystr = SiraNozlDT.Rows[0][L1Nozzle + lno - 1].ToString().TrimEnd();
                    //string tanknostr = keystr.Substring(DATAOFFS * (keyno - 1) + 2 * nzlno, 2);
                    string tanknostr = keystr.Substring(DATAOFFS * (keyno - 1) + 2 * (nzlno - 1), 2);
                    if (int.TryParse(tanknostr, out tankno) == true)
                        return tankno;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                Console.WriteLine("line " + lno.ToString() + " keiryo " + keyno.ToString() + " nzlno " + nzlno.ToString());
            }
            return tankno;
        }
    }
}
