﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace SiraPtlMlSysProcessor
{
    class MblrtChkClass
    {
        public bool IsWorking { get; set; }
        public MblrtChkClass()
        {
            IsWorking = true;
        }
        public void MblrtChkThread_Main()
        {
            try
            {
                LogData.WriteLog("モバイルルーター動作チェック開始");
                MRouterTable mrtble = new MRouterTable();
                mrtble.OpenTable("*");
                int numsite = mrtble.GetNumOfRecord();
                DateTime dtnow = DateTime.Now.ToLocalTime();
                string nodatasite = "";
                int nodatsite = 0;
                for (int i = 0; i < numsite; i++)
                {
                    try
                    {
                        string dstr;
                        dstr = "20" + mrtble.GetZaikoString(i).Substring(0, 10);
                        dstr = dstr.Insert(10, ":").Insert(8, " ").Insert(6, "/").Insert(4, "/");
                        DateTime dt = DateTime.Parse(dstr);
                        TimeSpan ts = dtnow - dt;
                        if (ts.TotalHours > 2)
                        {
                            nodatasite += mrtble.GetSSName(i) + ", ";
                            nodatsite += 1;
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.ToString());
                    }
                }
                if (nodatsite > 0) //データ更新サイトがある場合は通知
                {
                    SiraPtlUserProfile spuser = new SiraPtlUserProfile();
                    SendMail.SendMailExec("データ更新なしSS " + nodatasite, GlobalVar.MASTERMLADR);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            IsWorking = false;
        }
    } 
}
