﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace SiraPtlMlSysProcessor
{
    class GlobalVar
    {
		//グローバル変数
        public static string DBCONNECTION = "Data Source = skkatgsdb.database.windows.net;Initial Catalog = SkkAtgsDB;User ID=skkkaiadmin;Password = skkkaidb-201806";
        public static int NUMTANK = 20;
        public static string SIRACHKTIME = "0800";
        public static string MBLRTCHKTIME1 = "1100";
        public static string MBLRTCHKTIME2 = "1700";
        public static string MASTERMLADR = "n-matsuda@showa-kiki.co.jp";
        public static string AikosekiyuCmpcode = "JS0009";
        //public const int DataRetentionHour = 720 * 2; //24x30x2 2ヶ月
        public const int DataRetentionHour = 24 * 30; //24x30x2 40日分
        public const int ZHistRetentionHour = 720 * 6; //6ヶ月　在庫履歴

        //------ 対象会社名 -------------------
        public const int CompanySKKNo = 0;
        public const int CompanyAikoNo = 1;
        public const int CompanyEneosNo = 2;
        public const int CompanyShowakosanNo = 3;
        public const int CompanyKygnusNo = 4;
        public const int CompanyJxtgNo = 5;
        public static string GetCompanyName(int compno)
        {
            switch (compno)
            {
                case CompanySKKNo:
                default:
                    return SKKCompanyName;
                case CompanyAikoNo:
                    return AikoCompanyName;
                case CompanyEneosNo:
                    return EneCompanyName;
                case CompanyKygnusNo:
                    return KygCompanyName2;
                case CompanyJxtgNo:
                    return JxtgCompanyName2;
            }
        }

        public static string GetDefGWWcSkkcode(int compno)
        {
            switch (compno)
            {
                case CompanySKKNo:
                default:
                    return SKKCompGWUSkkcode;
                case CompanyAikoNo:
                    return AikoCompGWUSkkcode;
                case CompanyEneosNo:
                    return EneCompGWUSkkcode;
            }
        }
        public static string GetDefGwWcSkkcodeByCompname(string compname)
        {
            if (compname == AikoCompanyName)
                return AikoCompGWUSkkcode;
            else if (compname == EneCompanyName)
                return EneCompGWUSkkcode;
            else
                return SKKCompGWUSkkcode;
        }
        public static string GetDefWcSkkcode(int compno)
        {
            switch (compno)
            {
                case CompanySKKNo:
                default:
                    return SKKCompMBUSkkcode;
                case CompanyAikoNo:
                    return AikoCompMBUSkkcode;
                case CompanyEneosNo:
                    return EneCompMBUSkkcode;
            }
        }
        public static string GetDefWcSkkcodeByCompname(string compname)
        {
            if (compname == AikoCompanyName)
                return AikoCompMBUSkkcode;
            else if (compname == EneCompanyName)
                return EneCompMBUSkkcode;
            else
                return SKKCompMBUSkkcode;
        }

        public static string GetDefSkkcode(int compno)
        {
            switch (compno)
            {
                case CompanySKKNo:
                default:
                    return SKKDefSkkcode1;
                case CompanyAikoNo:
                    return AikoDefSkkcode1;
                case CompanyEneosNo:
                    return EneDefSkkcode1;
            }
        }

        public static string GetDefSitename(int compno)
        {
            switch (compno)
            {
                case CompanySKKNo:
                default:
                    return SKKDefSiteName1;
                case CompanyAikoNo:
                    return AikoDefSiteName1;
                case CompanyEneosNo:
                    return EneDefSiteName1;
            }
        }

        public static string MlSysCompanyName = "SKK";
        public static string MlCompanyName = "SKK";
        //------- SKK -------------------------------
        public static string SKKCompanyName = "SKK";
        public static string SKKCompanyName2 = "SKK";
        public static string SKKCompMBUSkkcode = "M099";
        public static string SKKDefSkkcode1 = "M099000001";
        public static string SKKDefSiteName1 = "SKKテスト";
        public static string SKKCompGWUSkkcode = "A099";
        public static string SKKAdminName = "SKKPrimeAdmin";
        public static string SKKSubAdminName = "SKKSubAdmin";
        public static int SkkMaxTankNo = 8;

        public static string SKKCompanyName3 = "SKK";
        public static string SKKMlSysCompanyName = "SKK";
        public static string SKKMlCompanyName = "Skk";
        public static string SKKCompanyFolder = "SkkKaihatsu";
        public static string SKKMlAdr1 = "n-matsuda@showa-kiki.co.jp";

        //-------- 相光石油 -----------------------------
        public static string AikoCompanyName = "AikoSekiyu";
        public static string AikoCompanyName2 = "相光石油";
        public static string AikoCompMBUSkkcode = "M012";
        public static string AikoDefSkkcode1 = "M012000001";
        public static string AikoDefSiteName1 = "大江SS";
        public static string AikoCompGWUSkkcode = "A060";
        public static string AikoSubAdminName = "AikoSubAdmin";
        public static int AikoMaxTankNo = 9;

        public static string AikoCompanyName3 = "相光石油";
        public static string AikoMlSysCompanyName = "相光石油";
        public static string AikoMlCompanyName = "相光石油";
        public static string AikoCompanyFolder = "Test1";

        //-------- エネオス株式会社 -----------------------------
        public static string EneCompanyName = "EneosCorp";
        public static string EneCompanyName2 = "エネオス株式会社";
        public static string EneCompMBUSkkcode = "M013";
        public static string EneDefSkkcode1 = "M013000001";
        public static string EneDefSiteName1 = "青森インターSS";
        public static string EneCompGWUSkkcode = "A099";
        public static string EneSubAdminName = "EneosSubAdmin";
        public static int EneMaxTankNo = 11;

        //-------------- キグナス石油 -------------------------------
        public static string KygCompanyName = "KygnusOil";
        public static string KygCompanyName2 = "エネオス石油販売株式会社";
        public static string KygCompMBUSkkcode = "M00";
        public static string KygDefSkkcode1 = "M000200015";
        public static string KygDefSiteName1 = "セルフ川越的場";
        public static string KygCompGWUSkkcode = "A099";
        public static string KygSubAdminName = "KygnusSubAdmin";
        public static int KygMaxTankNo = 11;

        //-------------- JXTGエネルギー -----------------------------
        public static string JxtgCompanyName = "JXTGEnergy";
        public static string JxtgCompanyName2 = "JXTGエネルギー株式会社";
        public static string JxtgCompMBUSkkcode = "M1";
        public static string JxtgDefSkkcode1 = "M100000001";
        public static string JxtgDefSiteName1 = "DDセルフ赤間SS";
        public static string JxtgCompGWUSkkcode = "A099";
        public static string JxtgSubAdminName = "JxtgSubAdmin";
        public static int JxtgMaxTankNo = 11;

    }

    public enum WrgType
    {
        LowWrg = 0, //減
        HighWrg, 　//満
        WtrWrg,    //水
        SnsErrWrg, //センサー異常
        LC4Wrg,    //漏えい点検
        LeakWrg,   //リーク
        NoDataWrg  //データ無し
    }

    public class ErrDataDetail
    {
        public string SiteName;    //施設名
        public string Skkcode;     //SKKコード
        public string Companyname;  //会社名
        public int OilTypeno;       //液種名
        public int TankNo;          //タンク番号
        public string DateStr;      //日時
        public int WrgType;         //警報種類
        public int TrgOrRel;        //0:警報解除 1:発生
        public int WtrLvl;          //水位
        public int WtrVol;          //水量
        public string LC4Res;       //漏えい点検結果,判定値

        public ErrDataDetail(string SName, string Scode, string Cname, int Ono, int Tno, string DStr, int WType, int ToR, int Wl, int Wv, string res)
        {
            SiteName = SName;
            Skkcode = Scode;
            Companyname = Cname;
            OilTypeno = Ono;
            TankNo = Tno;
            DateStr = DStr;
            WrgType = WType;
            TrgOrRel = ToR;
            WtrLvl = Wl;
            WtrVol = Wv;
            LC4Res = res;
        }
    }

    public class OilName
    {
        public static string GetOilName(int oiltype)
        {
            string oilname = "";
            switch (oiltype)
            {
                case 1:
                    return "ハイオク";
                case 2:
                default:
                    return "レギュラー";
                case 3:
                    return "灯油";
                case 4:
                    return "軽油";
            }
            return oilname;
        }
    }

    public class DataTableCtrl
    {
        static public void InitializeTable(DataTable dt)
        {
            if (dt != null)
            {
                dt.Clear();
                dt.Dispose();
                dt = null;
            }
        }

        static public void InitializeDataSet(DataSet ds)
        {
            if (ds != null)
            {
                ds.Clear();
                ds.Tables.Clear();
                ds.Dispose();
                ds = null;
            }
        }
    }

    public class DBCtrl
    {
        static public void ExecSelectAndFillTable(string sqlstr, DataTable dt)
        {
            string cnstr = GlobalVar.DBCONNECTION;
            System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
            System.Data.SqlClient.SqlDataAdapter dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
            dAdp.Fill(dt);
            cn.Close();
        }
        static public void ExecNonQuery(string sqlstr)
        {
            string cnstr = GlobalVar.DBCONNECTION;
            System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
            cn.Open();
            System.Data.SqlClient.SqlCommand Com;
            Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
            Com.CommandTimeout = 180;
            Com.ExecuteNonQuery();
            cn.Close();
        }

    }

}
