﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Diagnostics;
using System.Threading.Tasks;

namespace SiraPtlMlSysProcessor
{
    class SIRACompany
    {
        private DataSet CompanyTableDS;
        private DataTable CompanyTableDT;
        private const string stCompanyTbl = "カンパニーテーブル";
        private const int CNameColNo = 1;　//会社名
        private const int CCodeColNo = 2;  //CompanyCode
        private const int CUserNameColNo = 3; //LOBサーバー用ユーザー名
        private const int CPassWordColNo = 4; //LOBサーバー用パスワード
        private const int CFolderNameColNo = 5; //WillNetサーバー上フォルダー名
        private const int CMailAddress = 6; //メールアドレス
        private const int CMailAddress2 = 7; //メールアドレス

        //コンストラクター
        public SIRACompany()
        {
            if (CompanyTableDS != null)
            {
                CompanyTableDS.Clear();
                CompanyTableDS.Tables.Clear();
                CompanyTableDS.Dispose();
                CompanyTableDS = null;
            }
        }

        //会社テーブル読み込み
        public void OpenTable(string compname)
        {
            try
            {
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                string sqlstr;
                if (compname == "*")
                    sqlstr = "SELECT * FROM SIRACompany";
                else
                    sqlstr = "SELECT * FROM SIRACompany WHERE CompanyName = '" + compname + "'"; ;
                System.Data.SqlClient.SqlDataAdapter dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                if (CompanyTableDS != null)
                {
                    CompanyTableDS.Clear();
                    CompanyTableDS.Tables.Clear();
                    CompanyTableDS.Dispose();
                    CompanyTableDS = null;
                }
                CompanyTableDS = new DataSet("CompanyTable");
                CompanyTableDS.Tables.Add(stCompanyTbl);
                CompanyTableDT = CompanyTableDS.Tables[stCompanyTbl];

                dAdp.Fill(CompanyTableDS, stCompanyTbl);
                cn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }


        //レコード数取得
        public int GetNumOfRecord()
        {
            return CompanyTableDT.Rows.Count;
        }

        //会社名一覧取得
        public List<string> GetCompanyNames()
        {
            List<string> list = new List<string>();
            for (int i = 0; i < CompanyTableDT.Rows.Count; i++)
            {
                list.Add(CompanyTableDT.Rows[i][CNameColNo].ToString().TrimEnd());
            }
            return list;
        }

        //行番号から会社名取得
        public string GetCompanyName(int lineno)
        {
            string cpname = "";
            if ((CompanyTableDT != null) && (lineno < CompanyTableDT.Rows.Count))
            {
                return CompanyTableDT.Rows[lineno][CNameColNo].ToString().TrimEnd();
            }
            return cpname;
        }

        //行番号からカンパニーコード取得
        public string GetCompanyCode(int lineno)
        {
            string cpname = "";
            if ((CompanyTableDT != null) && (lineno < CompanyTableDT.Rows.Count))
            {
                return CompanyTableDT.Rows[lineno][CCodeColNo].ToString().TrimEnd();
            }
            return cpname;
        }

        //行番号からLOBサーバーユーザー名取得
        public string GetUserName(int lineno)
        {
            string cpname = "";
            if ((CompanyTableDT != null) && (lineno < CompanyTableDT.Rows.Count))
            {
                return CompanyTableDT.Rows[lineno][CUserNameColNo].ToString().TrimEnd();
            }
            return cpname;
        }

        //行番号からLOBサーバーパスワード名取得
        public string GetPassword(int lineno)
        {
            string cpname = "";
            if ((CompanyTableDT != null) && (lineno < CompanyTableDT.Rows.Count))
            {
                return CompanyTableDT.Rows[lineno][CPassWordColNo].ToString().TrimEnd();
            }
            return cpname;
        }

        //WillNetサーバー上フォルダー名取得
        public string GetFolderName(int lineno)
        {
            string cpname = "";
            if ((CompanyTableDT != null) && (lineno < CompanyTableDT.Rows.Count))
            {
                return CompanyTableDT.Rows[lineno][CFolderNameColNo].ToString().TrimEnd();
            }
            return cpname;
        }

        //メールアドレス取得
        public string GetMailAddress(int lineno)
        {
            string mladr = "";
            if ((CompanyTableDT != null) && (lineno < CompanyTableDT.Rows.Count))
            {
                return CompanyTableDT.Rows[lineno][CMailAddress].ToString().TrimEnd();
            }
            return mladr;
        }
        //メールアドレス2取得
        public string GetMailAddress2(int lineno)
        {
            string mladr = "";
            if ((CompanyTableDT != null) && (lineno < CompanyTableDT.Rows.Count))
            {
                return CompanyTableDT.Rows[lineno][CMailAddress2].ToString().TrimEnd();
            }
            return mladr;
        }
    }
}
