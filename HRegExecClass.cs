﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiraPtlMlSysProcessor
{
    //在庫履歴登録用クラス
    class HRegExecClass
    {
        const int ZSAVEINTERVAL = 6; //在庫保存間隔
        public bool IsWorking { get; set; }

        public HRegExecClass()
        {
            IsWorking = true;
        }

        public void HRegExecThread_Main()
        {
            DateTime curdt = DateTime.Now.ToLocalTime();
            int ihour = curdt.Hour;
            int imin = curdt.Minute;

            //6時間おきチェック　そうでなければリターン
#if true
            if (((ihour % ZSAVEINTERVAL) > 0) || (imin >= 10))
            {
                IsWorking = false;
                return;
            }
#endif
            DateTime nowdt = new DateTime(curdt.Year, curdt.Month, curdt.Day, curdt.Hour, 0, 0, curdt.Kind);
            LogData.WriteLog(curdt.ToString("yyyy/MM/dd HH:mm") + " 6時間保存処理開始");
            try
            {
                MRouterTable mrt = new MRouterTable();
                mrt.OpenTable("*");
                string zdata = "";
                string timest = "";
                string shour = "";
                string skkcode = "";
                DateTime timedt;

                for (int i = 0; i < mrt.GetNumOfRecord(); i++)
                {
                    try
                    {
                        skkcode = mrt.GetSkkcode(i);
                        zdata = mrt.GetZaikoString(i);
                        timest = "20" + zdata.Substring(0, 10);
                        timest = timest.Insert(10, ":").Insert(8, " ").Insert(6, "/").Insert(4, "/");
                        timedt = DateTime.Parse(timest);
                        TimeSpan ts = nowdt - timedt;
                        if( Math.Abs(ts.TotalMinutes)  < 10 )
                        {
                            SaveZaikoHistory(zdata, skkcode);
                        }
                        else 
                        {
                            //一つ前のデータを取り出す
                            LogData.WriteLog(skkcode + " " + timest + " 6時間保存処理10分越え");
                            string zdata2 = mrt.GetZaikoBeforeBuf(i); //10分前のデータを取り出す
                            string timest2 = "20" + zdata.Substring(0, 10);
                            timest2 = timest2.Insert(10, ":").Insert(8, " ").Insert(6, "/").Insert(4, "/");
                            DateTime timedt2 = DateTime.Parse(timest2);
                            TimeSpan ts2 = nowdt - timedt2;
                            //一つ前のデータが10分以内ならばそれを保存
                            if (Math.Abs(ts2.TotalMinutes) < 10)
                            {
                                SaveZaikoHistory(zdata2, skkcode);
                                LogData.WriteLog("Retry " + skkcode + " " + timest2 + " 6時間保存処理10分以内");
                            }
                            else
                            {   //一つ前のデータの差分がかえって大きいのであれば、結局最新のデータが20分以内なら保存する。
                                if(  Math.Abs(ts.TotalMinutes) < 20 )
                                {
                                    SaveZaikoHistory(zdata, skkcode);
                                    LogData.WriteLog("Retry " + skkcode + " " + timest + " 6時間保存処理20分以内");
                                } else
                                {
                                    LogData.WriteLog("Retry " + skkcode + " " + timest + " 6時間保存処理失敗20分越え");
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(skkcode + ex.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            curdt = DateTime.Now.ToLocalTime();
            LogData.WriteLog(curdt.ToString("yyyy/MM/dd HH:mm") + " 6時間保存処理終了");
            IsWorking = false;
        }

        //在庫の保存
        private bool SaveZaikoHistory(string zdata, string skkcode)
        {
            try
            {
                string timedt = "20" + zdata.Substring(0, 10);
                string zval;
                int[] ZaikoArray = new int[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };　//各タンク在庫量
                int[] WtrlvlArray = new int[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };　//各タンク水位
                int[] WtrvolArray = new int[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };　//各タンク水量

                ZaikoStr zs = new ZaikoStr(zdata);
                zs.Analyze();
                int iTankNum = zs.numtank;
                for (int i = 0; i < iTankNum; i++)
                {
                    zval = zs.vollst[i]; //各タンクの在庫量
                    ZaikoArray[i] = int.Parse(zval) * 100;
                    if (zs.bwtrinf == true)
                    {
                        zval = zs.wtrlvllst[i]; //各タンクの水位
                        WtrlvlArray[i] = int.Parse(zval);
                        zval = zs.wtrvollst[i]; //各タンクの水量
                        WtrvolArray[i] = int.Parse(zval);
                    }
                    //iTankNum++;
                }
                DateTime dt = DateTime.Now.ToLocalTime();
                TimeSpan ts = dt - new DateTime(2000, 1, 1, 0, 0, 0);
                int count = (int)ts.TotalDays;
                //各タンク在庫量をDBに登録
                string sqlstr = "INSERT INTO InvHistory (ID, SKKコード,日付";
                for (int i = 1; i <= iTankNum; i++)
                {
                    sqlstr += ",タンク" + i.ToString() + "在庫量";
                }
                sqlstr = sqlstr + ") VALUES ('" + count + "','" + skkcode + "','" + timedt + "'";
                for (int i = 0; i < iTankNum; i++)
                {
                    sqlstr = sqlstr + ",'" + ZaikoArray[i].ToString() + "'";
                }
                sqlstr = sqlstr + ")";
                DBCtrl.ExecNonQuery(sqlstr);

                if (zs.bwtrinf == true)
                {
                    //各タンク在庫量をDBに登録
                    sqlstr = "INSERT INTO WtrHistory (ID, SKKコード,日付";
                    for (int i = 1; i <= iTankNum; i++)
                    {
                        sqlstr += ",タンク" + i.ToString() + "水位,タンク" + i.ToString() + "水量";
                    }
                    sqlstr = sqlstr + ") VALUES ('" + count + "','" + skkcode + "','" + timedt + "'";
                    for (int i = 0; i < iTankNum; i++)
                    {
                        sqlstr = sqlstr + ",'" + WtrlvlArray[i].ToString() + "','" + WtrvolArray[i].ToString() + "'";
                    }
                    sqlstr = sqlstr + ")";
                    DBCtrl.ExecNonQuery(sqlstr);
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                if (skkcode == "M100000001")
                    LogData.WriteLog(skkcode + " 6時間在庫書き込み失敗");
                return false;
            }
        }

    }
}
