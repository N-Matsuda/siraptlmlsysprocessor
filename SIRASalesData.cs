﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using System.Diagnostics;
using System.Configuration;

namespace SiraPtlMlSysProcessor
{
    class SIRASalesData
    {
        private DataTable SIRASalesDT;      //販売、荷卸し生データテーブル
        private DataTable SalesDT;          //販売量データテーブル
        private DataTable DelivDT;          //荷卸し量データテーブル
        private const int IdColNo = 0;
        private const int SKKCodeColNo = 1;     //SKKコード
        private const int DateColNo = 2;        //日付
        private const int LeveDataColNo = 3;    //レべビジョンデータ
        private string skkcode;

        //荷卸しデータ
        const int NUMDLVDT = 19;    //荷卸しデータのサイズ
        const int DLVTNKOFFS = 1;   //タンク番号のオフセット
        const int DLVVOLOFFS = 3; //荷卸し量のオフセット
        const int DLVTMOFFS = 9;  //荷卸し時間のオフセット

        //販売データ
        const int DATAOFFS = 12;    //データ開始点
        const int NUMSALDT = 30;    //販売データのサイズ
        const int SALELNOFFS = 1;   //計量器ライン番号のオフセット
        const int SALELNOSIZE = 1;   //計量器ライン番号のサイズ
        const int SALENOOFFS = 2;   //計量器番号のオフセット
        const int SALENOSIZE = 2;   //計量器番号のサイズ
        const int SALENZLOFFS = 4;   //ノズル番号のオフセット
        const int SALENZLSIZE = 1;   //ノズル番号のサイズ
        const int SALESTMOFFS = 5;   //販売開始時間のオフセット
        const int SALESTMSIZE = 10;  //販売開始時間のサイズ
        const int SALEETMOFFS = 15;  //販売終了時間のオフセット
        const int SALEETMSIZE = 8;  //販売終了時間のサイズ
        const int SALEVOLOFFS = 23;  //販売量のオフセット
        const int SALEVOLSIZE = 6;   //販売量のサイズ

        public SIRASalesData()
        {
            DataTableCtrl.InitializeTable(SIRASalesDT);
        }

        //テーブル読み込み日付指定
        public void OpenTableWDate(string skcode, DateTime stdate, DateTime enddt)
        {
            try
            {
                //Stopwatch sw = new Stopwatch();
                //sw.Start();
                skkcode = skcode;
                string sqlstr = "SELECT * FROM SIRASalesData WHERE SKKコード= '" + skkcode + "' AND 日付 >='" + stdate.ToString("yyyyMMddHHmm") + "' AND 日付 <= '" + enddt.ToString("yyyyMMddHHmm") + "' ORDER BY 日付 ASC";
                DataTableCtrl.InitializeTable(SIRASalesDT);
                SIRASalesDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SIRASalesDT);
                //sw.Stop();
                //Debug.WriteLine(sw.Elapsed);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                //retry
                try
                {
                    string sqlstr = "SELECT * FROM SIRASalesData WHERE SKKコード= '" + skkcode + "' AND 日付 >='" + stdate.ToString("yyyyMMddHHmm") + "' AND 日付 <= '" + enddt.ToString("yyyyMMddHHmm") + "' ORDER BY 日付 ASC";
                    DataTableCtrl.InitializeTable(SIRASalesDT);
                    SIRASalesDT = new DataTable();
                    DBCtrl.ExecSelectAndFillTable(sqlstr, SIRASalesDT);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Retry failed " + e.ToString());
                }
            }
        }
        //レコード数取得
        public int GetNumOfRecord()
        {
            try
            {
                return SIRASalesDT.Rows.Count;

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return 0;
            }
        }
        //販売データ取得
        public string GetSalesData(int lineno)
        {
            try
            {
                if ((SIRASalesDT != null) && (lineno < SIRASalesDT.Rows.Count))
                {
                    return SIRASalesDT.Rows[lineno][LeveDataColNo].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }
        public string GetSalesString()
        {
            //1(計量器ライン番号) + 2(計量器番号) + 1(ノズル番号) + 10(開始時間) + 8(終了時間) + 6(販売量)
            string retstr = "計量器ライン番号,計量器番号,ノズル番号,開始時間,終了時間,販売量\r\n";
            SiraNzlInfo snzinf = new SiraNzlInfo(); //DBのノズル情報を開く
            snzinf.OpenTable(skkcode);
            if (snzinf.GetNumOfRecord() > 0) //ノズル情報がある場合
            {
                retstr = "計量器ライン番号,計量器番号,ノズル番号,タンク番号, 開始時間,終了時間,販売量\r\n";
            }
            try
            {
                //ここで実際のデータを解析して、配送、販売、在庫データを対応する液種に挿入する。
                string recstr;          //配送、販売、在庫各レコード文字列
                string itemstr;         //各レコード内項目文字列
                DateTime dt;
                int numcnt = GetNumOfRecord();
                string mkstr;

                for (int i = 0; i < numcnt; i++)
                {
                    DateTime dtnow = DateTime.Now;
                    string sdata = GetSalesData(i);
                    int pos = DATAOFFS;
                    int slen = sdata.Length;
                    while (true)
                    {
                        if (pos >= slen)
                            break;
                        try
                        {
                            mkstr = sdata.Substring(pos, 1);  //データ種類取り出し
                            recstr = sdata.Substring(pos, NUMSALDT);
                            switch (mkstr)
                            {
                                default:
                                case "A":                   //液面計データ
                                    break;
                                case "D":                   //荷卸しデータ
                                    break;
                                case "S":                   //販売データ
                                    //計量器ライン番号
                                    itemstr = recstr.Substring(SALELNOFFS, SALELNOSIZE);
                                    int lno = int.Parse(itemstr) + 1;
                                    retstr += lno.ToString() + ",";
                                    //計量器番号
                                    itemstr = recstr.Substring(SALENOOFFS, SALENOSIZE);
                                    int kno = int.Parse(itemstr) + 1;
                                    retstr += kno.ToString() + ",";
                                    //ノズル番号
                                    itemstr = recstr.Substring(SALENZLOFFS, SALENZLSIZE);
                                    int nzlno = int.Parse(itemstr);
                                    retstr += itemstr + ",";

                                    //タンク番号取り出し
                                    int tno = snzinf.GetNzlTankNo(lno, kno, nzlno);
                                    if (tno > 0)
                                        retstr += tno.ToString() + ",";

                                    //開始時間取り出し
                                    itemstr = "20" + recstr.Substring(SALESTMOFFS, SALESTMSIZE) + "00"; //開始時間取り出し
                                    dt = DateTime.ParseExact(itemstr, "yyyyMMddHHmmss", System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.None);
                                    retstr += dt.ToString("yyyy/MM/dd HH:mm") + ",";

                                    //終了時間取り出し
                                    itemstr = dtnow.ToString("yyyy") + recstr.Substring(SALEETMOFFS, SALEETMSIZE) + "00"; //終了時間取り出し
                                    dt = DateTime.ParseExact(itemstr, "yyyyMMddHHmmss", System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.None);
                                    retstr += dt.ToString("yyyy/MM/dd HH:mm") + ",";

                                    //販売量
                                    itemstr = recstr.Substring(SALEVOLOFFS, SALEVOLSIZE);
                                    itemstr = itemstr.Insert(SALEVOLSIZE - 2, ".");
                                    retstr += itemstr + "\r\n";
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                            break;
                        }
                        pos += NUMSALDT;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return retstr;
        }

        //レギュラー販売履歴Datatable作成
        public (DataTable, DataTable) CreateSalesDT()
        {
            SalesDT = new DataTable(); //販売データテーブル
            string[] columnlst = { "タンク番号", "開始時間", "終了時間", "販売量" };
            SalesDT.Columns.AddRange(columnlst.Select(n => new DataColumn(n)).ToArray());
            DelivDT = new DataTable();
            string[] columnlst2 = { "タンク番号", "終了時間", "荷卸量" };
            DelivDT.Columns.AddRange(columnlst2.Select(n => new DataColumn(n)).ToArray());

            SiraNzlInfo snzinf = new SiraNzlInfo(); //DBのノズル情報を開く
            snzinf.OpenTable(skkcode);

            try
            {
                //ここで実際のデータを解析して、配送、販売、在庫データを対応する液種に挿入する。
                string recstr;          //配送、販売、在庫各レコード文字列
                string itemstr;         //各レコード内項目文字列
                DateTime dt;
                int numcnt = GetNumOfRecord();
                string mkstr;

                for (int i = 0; i < numcnt; i++)
                {
                    DateTime dtnow = DateTime.Now;
                    string sdata = GetSalesData(i);
                    int pos = DATAOFFS;
                    int slen = sdata.Length;
                    while (true)
                    {
                        if (pos >= slen)
                            break;
                        try
                        {
                            mkstr = sdata.Substring(pos, 1);  //データ種類取り出し
                            switch (mkstr)
                            {
                                default:
                                case "A":                   //液面計データ
                                    break;
                                case "D":                   //荷卸しデータ
                                    recstr = sdata.Substring(pos, NUMDLVDT);
                                    DataRow dvrow = DelivDT.NewRow();
                                    //タンク番号
                                    itemstr = recstr.Substring(DLVTNKOFFS, 2);
                                    int dtno = int.Parse(itemstr);
                                    dvrow["タンク番号"] = dtno.ToString();
                                    //荷卸し量
                                    itemstr = recstr.Substring(DLVVOLOFFS, 6);
                                    int dvol = int.Parse(itemstr);
                                    dvrow["荷卸量"] = dvol.ToString();
                                    //荷卸し時間
                                    itemstr = "20" + recstr.Substring(DLVTMOFFS, 10) + "00";
                                    dt = DateTime.ParseExact(itemstr, "yyyyMMddHHmmss", System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.None);
                                    itemstr = dt.ToString("yyyy/MM/dd HH:mm");
                                    dvrow["終了時間"] = itemstr;
                                    DelivDT.Rows.Add(dvrow);
                                    pos += NUMDLVDT;    //荷卸しデータのサイズ
                                    break;
                                case "S":                   //販売データ
                                    recstr = sdata.Substring(pos, NUMSALDT);
                                    //計量器ライン番号 1-
                                    itemstr = recstr.Substring(SALELNOFFS, SALELNOSIZE);
                                    int lno = int.Parse(itemstr) + 1;
                                    //計量器番号 1-44
                                    itemstr = recstr.Substring(SALENOOFFS, SALENOSIZE);
                                    int kno = int.Parse(itemstr) + 1;
                                    //int kno = int.Parse(itemstr);
                                    //ノズル番号 1-3
                                    itemstr = recstr.Substring(SALENZLOFFS, SALENZLSIZE);
                                    int nzlno = int.Parse(itemstr);
                                    //タンク番号 
                                    int tno = snzinf.GetNzlTankNo(lno, kno, nzlno);
                                    if (tno != 0)
                                    {
                                        DataRow drow = SalesDT.NewRow();
                                        drow["タンク番号"] = tno.ToString();
                                        //開始時間取り出し
                                        itemstr = "20" + recstr.Substring(SALESTMOFFS, SALESTMSIZE) + "00"; //開始時間取り出し
                                        dt = DateTime.ParseExact(itemstr, "yyyyMMddHHmmss", System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.None);
                                        itemstr = dt.ToString("yyyy/MM/dd HH:mm");
                                        drow["開始時間"] = itemstr;

                                        //終了時間取り出し
                                        itemstr = dtnow.ToString("yyyy") + recstr.Substring(SALEETMOFFS, SALEETMSIZE) + "00"; //終了時間取り出し
                                        dt = DateTime.ParseExact(itemstr, "yyyyMMddHHmmss", System.Globalization.DateTimeFormatInfo.InvariantInfo, System.Globalization.DateTimeStyles.None);
                                        itemstr = dt.ToString("yyyy/MM/dd HH:mm");
                                        drow["終了時間"] = itemstr;

                                        //販売量
                                        itemstr = recstr.Substring(SALEVOLOFFS, SALEVOLSIZE);
                                        drow["販売量"] = itemstr;
                                        SalesDT.Rows.Add(drow);
                                        pos += NUMSALDT;
                                        break;
                                    }
                                    pos += NUMSALDT;
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                            //break;
                        }
                        //pos += NUMSALDT;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Debug.WriteLine("create sales datatable");
            //return DelivDT;
            return (SalesDT, DelivDT);
        }
        public int GetSalesDTNumOfLine()
        {
            return SalesDT.Rows.Count;
        }
        //指定されたタンクの指定時間内での販売量を求める
        public int GetSalesVolTime(int tno, string sttime, string edtime)
        {
            int totalvol = 0;
            try
            {
                //string sqlstr = "タンク番号 = '" + tno.ToString() + "' AND 開始時間 > '" + sttime + "' AND 終了時間 <= '" + edtime + "'";
                string sqlstr = "タンク番号 = '" + tno.ToString() + "'";
                DataRow[] drows = SalesDT.Select(sqlstr);
                int cnt = drows.Count();
                DateTime eddt = DateTime.Parse(edtime);
                DateTime stdt = DateTime.Parse(sttime);
                TimeSpan checkts = eddt - stdt;
                if (cnt > 0)
                {
                    for (int i = 0; i < cnt; i++)
                    {
                        string tmstr = drows[i]["終了時間"].ToString().TrimEnd();
                        DateTime sldt = DateTime.Parse(tmstr);
                        TimeSpan ts = eddt - sldt;
                        //if( (ts.TotalMinutes >= (double)0 ) && (ts.TotalMinutes < (double)10) )
                        if ((ts.TotalMinutes > 0) && (ts.TotalMinutes <= (double)checkts.TotalMinutes))
                        {
                            string svol = drows[i]["販売量"].ToString().TrimEnd();
                            int vol;
                            if (int.TryParse(svol, out vol) == true)
                                totalvol += vol / 100;
                        }
                    }
                    return totalvol;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return totalvol;
        }
        //指定されたタンクの指定時間内での荷卸し量を求める
        public int GetDelivVolTime(int tno, string sttime, string edtime)
        {
            int vol = 0;
            try
            {
                //string sqlstr = "タンク番号 = '" + tno.ToString() + "' AND 開始時間 > '" + sttime + "' AND 終了時間 <= '" + edtime + "'";
                string sqlstr = "タンク番号 = '" + tno.ToString() + "'";
                DataRow[] drows = DelivDT.Select(sqlstr);
                int cnt = drows.Count();
                DateTime eddt = DateTime.Parse(edtime);
                if (cnt > 0)
                {
                    for (int i = 0; i < cnt; i++)
                    {
                        string tmstr = drows[i]["終了時間"].ToString().TrimEnd();
                        DateTime sldt = DateTime.Parse(tmstr);
                        TimeSpan ts = eddt - sldt;
                        if ((ts.TotalMinutes >= (double)0) && (ts.TotalMinutes < (double)10))
                        {
                            string dvol = drows[i]["荷卸量"].ToString().TrimEnd();
                            vol += int.Parse(dvol);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return vol;
        }

        public string GetSalesStartTMByLine(int lineno)
        {
            try
            {
                if ((SalesDT != null) && (lineno < SalesDT.Rows.Count))
                {
                    return SalesDT.Rows[lineno][0].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }
        public string GetSalesEndTMByLine(int lineno)
        {
            try
            {
                if ((SalesDT != null) && (lineno < SalesDT.Rows.Count))
                {
                    return SalesDT.Rows[lineno][1].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }

        public string GetSalesVolByLine(int lineno)
        {
            try
            {
                if ((SalesDT != null) && (lineno < SalesDT.Rows.Count))
                {
                    return SalesDT.Rows[lineno][2].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }
        public static void DeleteOldData()
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            int hour = dt.Hour;
           // if (hour < 23)  //23時以降に実施
           //     return;

            try
            {
                dt = dt.AddHours(-1 * GlobalVar.DataRetentionHour); //テスト 60日前
                string sqlstr = "DELETE FROM SIRASalesData WHERE 日付 < '" + dt.ToString("yyyyMMddHHmm") + "'";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
