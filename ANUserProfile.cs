﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace SiraPtlMlSysProcessor
{
    class ANUserProfile
    {
        private static int SKKCODELENGTH = 1; //ANUserProfileのSKKコード欄の最低の長さ
        private DataTable ANUserDT;
        private DataTable TempDT;
        //コンストラクタ―
        public ANUserProfile()
        {

        }
        //テーブル読み込み SKKコード
        public void OpenTable(string skkcode)
        {
            try
            {
                string scode = skkcode.Substring(0, 1);
                string sqlstr = "SELECT メールアドレス,SKKコード FROM ANUserProfile　WHERE SKKコード LIKE '" + scode + "%'";
                DataTableCtrl.InitializeTable(TempDT);
                TempDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, TempDT);
                int num = TempDT.Rows.Count;

                ANUserDT = new DataTable();
                ANUserDT.Columns.Add(new DataColumn("メールアドレス", typeof(string)));
                ANUserDT.Columns.Add(new DataColumn("SKKコード", typeof(string)));
                for(int i=0; i<num; i++)
                {
                    scode = TempDT.Rows[i][1].ToString().TrimEnd();
                    if( skkcode.IndexOf(scode) >= 0 )
                        ANUserDT.ImportRow(TempDT.Rows[i]);
                }
                Console.WriteLine("End Of search");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        //レコード数取得
        public int GetNumOfRecord()
        {
            return ANUserDT.Rows.Count;
        }

        //メールアドレス取得
        public string GetMailAdr(int lineno)
        {
            try
            {
                if ((ANUserDT != null) && (lineno < ANUserDT.Rows.Count))
                {
                    return ANUserDT.Rows[lineno][0].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }

    }
}
