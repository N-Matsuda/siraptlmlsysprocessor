﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using System.Diagnostics;
using System.Configuration;

namespace SiraPtlMlSysProcessor
{
    class InvData
    {
        private DataTable InvDataDT;
        private DataTable ZaikoTable;
        private DataTable RegularInvDT;
        private const int NUMTANK = 20;
        private const int IdColNo = 0;
        private const int SKKCodeColNo = 1;     //SKKコード
        private const int DateColNo = 2;        //日付
        private const int LeveDataColNo = 3;    //レべビジョンデータ
        private const string stSSName = "配送先名"; // 配送先名   (1)
        private const string stDate = "日付";
        private const string stTime = "時刻";
        private const string stZaiko = "在庫";
        private const string stOilCode = "液種番号";
        private const string stFullVol = "全容量"; //全容量     
        private const string stTankStatus = "タンク状況";
        enum ZaikoCol
        {
            Date = 1, //日付
            Time = 2, //時刻
            Zaiko = 3, //在庫 3～22
            OilCode = Zaiko + NUMTANK, //油種 23～32
            FullVol = OilCode + NUMTANK, //全容量 33～52
            TankStatus = FullVol + NUMTANK  //状況　53～72
        }
        private int numtank;
        //コンストラクター
        public InvData()
        {
            DataTableCtrl.InitializeTable(InvDataDT);
        }

        //テーブル読み込み日付指定
        public void OpenTableWDate(string skkcode, DateTime stdate, DateTime enddt)
        {
            try
            {
                string sqlstr = "SELECT * FROM InvData WHERE SKKコード= '" + skkcode + "' AND 日付 >='" + stdate.ToString("yyyyMMddHHmm") + "' AND 日付 <= '" + enddt.ToString("yyyyMMddHHmm") + "' ORDER BY 日付 ASC";
                DataTableCtrl.InitializeTable(InvDataDT);
                InvDataDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, InvDataDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        //レコード数取得
        public int GetNumOfRecord()
        {
            try
            {
                return InvDataDT.Rows.Count;

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return 0;
            }
        }
        //在庫データ取得
        public string GetInvData(int lineno)
        {
            try
            {
                if ((InvDataDT != null) && (lineno < InvDataDT.Rows.Count))
                {
                    return InvDataDT.Rows[lineno][LeveDataColNo].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }

        //在庫テーブル(ZaikoTable)の作成 SS名、日付、時間、SSコード、各タンク在庫量、各タンク油種、各タンク容量
        public string CreateZaikoTable()
        {
            string zstr, dstring = "";
            string strwk, strwk2;
            int inv, capa;

            int numcnt = GetNumOfRecord();
            if (numcnt == 0)
                return dstring;
            DataTableCtrl.InitializeTable(ZaikoTable);
            //テーブルのフィールドを定義する
            ZaikoTable = new DataTable();
            string[] columnlst = { stSSName, stDate, stTime };
            ZaikoTable.Columns.AddRange(columnlst.Select(n => new DataColumn(n)).ToArray());

            for (int i = 1; i <= NUMTANK; i++)
            {
                ZaikoTable.Columns.Add(new DataColumn(stZaiko + i.ToString(), typeof(int)));
            }
            for (int i = 1; i <= NUMTANK; i++)
            {
                ZaikoTable.Columns.Add(new DataColumn(stOilCode + i.ToString(), typeof(int)));
            }
            for (int i = 1; i <= NUMTANK; i++)
            {
                ZaikoTable.Columns.Add(new DataColumn(stFullVol + i.ToString(), typeof(int)));
            }
            for (int i = 1; i <= NUMTANK; i++)
            {
                ZaikoTable.Columns.Add(new DataColumn(stTankStatus + i.ToString(), typeof(string)));
            }
            for (int i = 0; i < numcnt; i++)
            {
                zstr = "";

                DataRow drow = ZaikoTable.NewRow();

                string sts = "";
                zstr = GetInvData(i);           //在庫文字列取り出し
                if (zstr.Length > 10)
                {
                    drow[(int)ZaikoCol.Date] = "20" + zstr.Substring(0, 6); //在庫文字列より日付、時間取得
                    drow[(int)ZaikoCol.Time] = zstr.Substring(6, 4);
                    dstring = (string)drow[(int)ZaikoCol.Date] + (string)drow[(int)ZaikoCol.Time];
                }
                else
                {
                    continue;
                }
                ZaikoStr zs = new ZaikoStr(zstr);   //在庫文字列解析
                zs.Analyze();
                numtank = zs.numtank;
                for (int j = 0; j < zs.numtank; j++)
                {
                    try
                    {
                        //液種
                        strwk = zs.lqtypelst[j];
                        drow[(int)ZaikoCol.OilCode + j] = int.Parse(strwk);

                        //在庫量, タンク容量取得
                        inv = int.Parse(zs.vollst2[j]);
                        drow[(int)ZaikoCol.Zaiko + j] = inv;

                        //全容量
                        capa = int.Parse(zs.capalst[j]);
                        drow[(int)ZaikoCol.FullVol + j] = capa;
                        if (capa > 0)
                        {
                            //警報
                            strwk = zs.sslan2stat[j];
                            strwk2 = zs.sslanstat[j];
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
                //新規行を追加
                ZaikoTable.Rows.Add(drow);
            }
            Console.WriteLine("Done"); ;
            return dstring;
        }
        //任意行の文字列ストリング求める
        public string GetZaikoDateTime(int rowno)
        {
            string dtstr = "";
            try
            {
                if (rowno < InvDataDT.Rows.Count)
                {
                    dtstr = ZaikoTable.Rows[rowno][(int)ZaikoCol.Date].ToString().TrimEnd() + ZaikoTable.Rows[rowno][(int)ZaikoCol.Time].ToString().TrimEnd();
                    dtstr = dtstr.Insert(10, ":").Insert(8, " ").Insert(6, "/").Insert(4, "/");
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return dtstr;
        }
        //任意行の指定タンクの在庫文字列求める
        public string GetZaikoVol(int tankno, int rowno)
        {
            string zstr = "";
            try
            {
                if (tankno >= numtank)
                    return zstr;
                if (rowno < InvDataDT.Rows.Count)
                {
                    zstr = ZaikoTable.Rows[rowno][(int)ZaikoCol.Zaiko + tankno].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return zstr;
        }
        //任意行の指定タンクの液種文字列を求める
        public string GetOilType(int tankno)
        {
            string zstr = "";
            try
            {
                if (tankno >= numtank)
                    return zstr;
                zstr = OilName.GetOilName((int)ZaikoTable.Rows[0][(int)(int)ZaikoCol.OilCode + tankno]);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return zstr;
        }

        //在庫テーブル用文字列の作成  時間、タンク番号、各タンク在庫量、各タンク油種、各タンク容量 x 20
        public string GetInventoryString()
        {
            string retstr = "タンク番号,液種,在庫量,ステータス1,ステータス2,温度,容量,減警報値,水位,水量,LC-4,販売\r\n";
            string zstr = "";
            string strwk;
            int inv, capa;

            int numcnt = GetNumOfRecord();
            if (numcnt == 0)
                return retstr;

            for (int i = 0; i < numcnt; i++)
            {
                zstr = "";
                zstr = GetInvData(i);   //データテーブルより在庫文字列取得
                if (zstr.Length < 10)
                {
                    continue;
                }

                ZaikoStr zs = new ZaikoStr(zstr);   //在庫文字列解析
                zs.Analyze();

                retstr += zs.timestmp.ToString("yyyy/MM/dd HH:mm") + ",";
                for (int j = 0; j < zs.numtank; j++)
                {
                    try
                    {
                        //タンク番号
                        retstr += (j + 1).ToString() + ",";
                        //液種
                        strwk = zs.lqtypelst[j];
                        int oilno = int.Parse(strwk);
                        string olname = OilName.GetOilName(oilno);
                        retstr += olname + ",";
                        //在庫量
                        inv = int.Parse(zs.vollst2[j]);
                        retstr += inv.ToString() + ",";
                        //SS-LANステータス
                        retstr += zs.sslanstat[j] + ",";
                        //SS-LAN2ステータス
                        retstr += zs.sslan2stat[j] + ",";
                        //温度情報
                        retstr += zs.templst[j] + ",";
                        //申請容量
                        capa = int.Parse(zs.capalst[j]);
                        retstr += capa.ToString() + ",";
                        //減警報値
                        inv = int.Parse(zs.lowlmtlst[j]);
                        retstr += inv.ToString() + ",";
                        if (zs.bwtrinf == true) //水情報あり
                        {
                            //水位
                            inv = int.Parse(zs.wtrlvllst[j]);
                            retstr += inv.ToString() + ",";
                            //水量
                            inv = int.Parse(zs.wtrvollst[j]);
                            retstr += inv.ToString() + ",";
                            //漏えい点検情報
                            retstr += zs.lc4inflst[j] + ",";
                            //販売情報
                            inv = int.Parse(zs.salesvollst[j]);
                            retstr += inv.ToString() + ",";
                        }
                        else
                        {
                            retstr += ",,,,";
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
                //for (int k = zs.numtank; k <= NUMTANK; k++)
                //{
                //    retstr += ",,,,,,,,,,,";
                //}
                retstr += "\r\n";
            }
            Console.WriteLine("Done"); ;
            return retstr;
        }

        //================= 在庫履歴データテーブル作成 阪神芦屋 tank3,4,5 レギュラー連結設定用 ====================================
        public void GetRegularSalesDT()
        {
            RegularInvDT = new DataTable();
            string[] columnlst = { "時間", "在庫量" };
            RegularInvDT.Columns.AddRange(columnlst.Select(n => new DataColumn(n)).ToArray());

            try
            {
                //ここで実際のデータを解析して、配送、販売、在庫データを対応する液種に挿入する。
                string zstr;          //配送、販売、在庫各レコード文字列
                DateTime dt;
                int numcnt = GetNumOfRecord();
                for (int i = 0; i < numcnt; i++)
                {
                    zstr = "";
                    zstr = GetInvData(i);   //データテーブルより在庫文字列取得
                    if (zstr.Length < 10)
                    {
                        continue;
                    }
                    ZaikoStr zs = new ZaikoStr(zstr);   //在庫文字列解析
                    zs.Analyze();
                    DataRow drow = RegularInvDT.NewRow();
                    drow["時間"] = zs.timestmp.ToString("yyyyMMddHHmm");
                    int vol = int.Parse(zs.vollst2[3]) + int.Parse(zs.vollst2[4]) + int.Parse(zs.vollst2[5]);
                    drow["在庫量"] = vol.ToString();
                    RegularInvDT.Rows.Add(drow);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        public int GetRegularInvDTNumOfLine()
        {
            return RegularInvDT.Rows.Count;
        }
        public string GetRegularInvTimeByLine(int lineno)
        {
            try
            {
                if ((RegularInvDT != null) && (lineno < RegularInvDT.Rows.Count))
                {
                    return RegularInvDT.Rows[lineno][0].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }

        public string GetRegularInvByLine(int lineno)
        {
            try
            {
                if ((RegularInvDT != null) && (lineno < RegularInvDT.Rows.Count))
                {
                    return RegularInvDT.Rows[lineno][1].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }

        public static void DeleteOldData()
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            int hour = dt.Hour;
            if (hour < 23)  //23時以降に実施
                return;

            try
            {
                dt = dt.AddHours(-1 * GlobalVar.DataRetentionHour); //テスト 30日前
                string sqlstr = "DELETE FROM InvData WHERE 日付 < '" + dt.ToString("yyyyMMddHHmm") + "'";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
