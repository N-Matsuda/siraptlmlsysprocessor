﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace SiraPtlMlSysProcessor
{
    class SiteInfData
    {
        private DataTable SiteInfDT;
        private DataTable TankCapaDT;
        private DataTable LowVolumeDT;

        private int numtank;
        //コンストラクタ
        public SiteInfData()
        {
            DataTableCtrl.InitializeTable(SiteInfDT);
        }

        //テーブル読み込み SKKコード
        public int OpenTableSkkcode(string skkcode)
        {
            try
            {
                string sqlstr = "SELECT * FROM SiteInf WHERE SKKコード= '" + skkcode + "'";
                DataTableCtrl.InitializeTable(SiteInfDT);
                SiteInfDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SiteInfDT);

                int i;
                string otype;
                for (i = 0; i < GlobalVar.NUMTANK; i++)
                {
                    try
                    {
                        otype = (string)SiteInfDT.Rows[0][2 + 4 * i];
                    }
                    catch (Exception ex)
                    {
                        break;
                    }
                }
                numtank = i;
                return numtank;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return 0;
            }
        }

        //有効なタンクの数を返す
        public int GetNumTank()
        {
            return numtank;
        }
        
        //各タンクの液種配列を返答
        public string[] GetOilType()
        {
            string[] oilarr = new string[numtank];
            for (int i = 0; i < numtank; i++)
            {
                if (SiteInfDT.Rows[0][2 + 4 * i] == null)
                    break;
                oilarr[i] = SiteInfDT.Rows[0][2 + 4 * i].ToString();
            }
            return oilarr;
        }
        //指定タンクの液種
        public string GetOilTypeByTank(int tankno)
        {
            return SiteInfDT.Rows[0][2 + 4 * (tankno - 1)].ToString();
        }

        //各タンクの連結情報を返答
        public string[] GetLinkInf()
        {
            string[] linkarr = new string[numtank];
            for (int i = 0; i < numtank; i++)
            {
                try
                {
                    if (SiteInfDT.Rows[0][2 + 4 * i + 1] == null)
                        break;
                    linkarr[i] = SiteInfDT.Rows[0][2 + 4 * i + 1].ToString();
                }
                catch (Exception ex)
                {
                    break;
                }

            }
            return linkarr;
        }
        //指定タンクの連結情報
        public string GetLinkInfByTank(int tankno)
        {
            try
            {
                string linkinf = SiteInfDT.Rows[0][2 + 4 * (tankno - 1) + 1].ToString();
                string linkstr = "";
                byte[] bytearr = System.Text.Encoding.GetEncoding("ASCII").GetBytes(linkinf);
                byte blnk;
                for (int i = 0; i < 8; i++)
                {
                    blnk = bytearr[i];
                    if (blnk >= 0x60)
                        linkstr = "タンク20 ";
                    else if (blnk >= 0x50)
                        linkstr = "タンク1" + (blnk & 0xf).ToString() + " ";
                    else if (blnk > 0x40)
                        linkstr = "タンク" + (blnk & 0xf).ToString() + " ";
                }
                if (linkstr == "")
                    return "連結なし";
                else
                    return "連結:" + linkstr;
            }
            catch (Exception ex)
            {
                return "連結なし";
            }
        }
        //指定タンクのタンク数列
        public string GetTankNoStrWTankno(int tno)
        {
            string lnkinf = "";

            try
            {
                lnkinf = GetLinkInfByTank(tno);
                if (lnkinf.IndexOf("連結なし") >= 0)
                    lnkinf = (tno).ToString();
                else if (lnkinf.IndexOf("連結") >= 0)
                {
                    lnkinf = lnkinf.Substring(6);
                    lnkinf = lnkinf.Replace("タンク", "");
                }
                else
                    lnkinf = (tno).ToString();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return lnkinf;
        }

        //各タンクの容量
        public string[] GetCapacity()
        {
            string[] capaarr = new string[numtank];
            for (int i = 0; i < numtank; i++)
            {
                try
                {
                    if (SiteInfDT.Rows[0][2 + 4 * i + 2] == null)
                        break;
                    capaarr[i] = SiteInfDT.Rows[0][2 + 4 * i + 2].ToString();
                }
                catch (Exception ex)
                {
                    break;
                }
            }
            return capaarr;
        }
        //指定タンクの容量
        public int GetCapacityByTank(int tankno)
        {
            int capa = 0;
            try
            {
                capa = (int)SiteInfDT.Rows[0][2 + 4 * (tankno - 1) + 2];
            }
            catch (Exception ex)
            {
                ;
            }
            return capa;
        }

        //各タンクの減警報値
        public string[] GetLowWarning()
        {
            if( SiteInfDT.Rows.Count == 0 )
            {
                string[] lowar= new string[GlobalVar.NUMTANK];
                for (int i = 0; i < GlobalVar.NUMTANK; i++)
                {
                    lowar[i] = "0";
                }
                return lowar;
            }
            string[] lowarr = new string[numtank];
            for (int i = 0; i < numtank; i++)
            {
                try
                {
                    if (SiteInfDT.Rows[0][2 + 4 * i + 3] == null)
                        break;
                    lowarr[i] = SiteInfDT.Rows[0][2 + 4 * i + 3].ToString();
                }
                catch (Exception ex)
                {
                    ;
                }
            }
            return lowarr;
        }

        //指定タンクの下限値
        public int GetLowLimitByTank(int tankno)
        {
            int capa = 0;
            try
            {
                capa = (int)SiteInfDT.Rows[0][2 + 4 * (tankno - 1) + 3];
            }
            catch (Exception ex)
            {
                ;
            }
            return capa;
        }


        public DataTable GetCapaTable()
        {
            //会社メールアドレス表示
            TankCapaDT = new DataTable();
            string[] columnlst = { "ﾀﾝｸ1", "ﾀﾝｸ2", "ﾀﾝｸ3", "ﾀﾝｸ4", "ﾀﾝｸ5", "ﾀﾝｸ6", "ﾀﾝｸ7", "ﾀﾝｸ8", "ﾀﾝｸ9", "ﾀﾝｸ10" };
            TankCapaDT.Columns.AddRange(columnlst.Select(n => new DataColumn(n)).ToArray());

            DataRow dEditRow = TankCapaDT.NewRow();
            dEditRow[0] = SiteInfDT.Rows[0][2 + 2].ToString();
            dEditRow[1] = SiteInfDT.Rows[0][2 + 4 * 1 + 2].ToString();
            dEditRow[2] = SiteInfDT.Rows[0][2 + 4 * 2 + 2].ToString();
            dEditRow[3] = SiteInfDT.Rows[0][2 + 4 * 3 + 2].ToString();
            dEditRow[4] = SiteInfDT.Rows[0][2 + 4 * 4 + 2].ToString();
            dEditRow[5] = SiteInfDT.Rows[0][2 + 4 * 5 + 2].ToString();
            dEditRow[6] = SiteInfDT.Rows[0][2 + 4 * 6 + 2].ToString();
            dEditRow[7] = SiteInfDT.Rows[0][2 + 4 * 7 + 2].ToString();
            dEditRow[8] = SiteInfDT.Rows[0][2 + 4 * 8 + 2].ToString();
            dEditRow[9] = SiteInfDT.Rows[0][2 + 4 * 9 + 2].ToString();
            TankCapaDT.Rows.Add(dEditRow);
            return TankCapaDT;
        }

        public DataTable GetLWrgTable()
        {
            LowVolumeDT = new DataTable();
            string[] columnlst = { "ﾀﾝｸ1", "ﾀﾝｸ2", "ﾀﾝｸ3", "ﾀﾝｸ4", "ﾀﾝｸ5", "ﾀﾝｸ6", "ﾀﾝｸ7", "ﾀﾝｸ8", "ﾀﾝｸ9", "ﾀﾝｸ10" };
            LowVolumeDT.Columns.AddRange(columnlst.Select(n => new DataColumn(n)).ToArray()); DataRow dEditRow = LowVolumeDT.NewRow();

            dEditRow[0] = SiteInfDT.Rows[0][2 + 3].ToString();
            dEditRow[1] = SiteInfDT.Rows[0][2 + 4 * 1 + 3].ToString();
            dEditRow[2] = SiteInfDT.Rows[0][2 + 4 * 2 + 3].ToString();
            dEditRow[3] = SiteInfDT.Rows[0][2 + 4 * 3 + 3].ToString();
            dEditRow[4] = SiteInfDT.Rows[0][2 + 4 * 4 + 3].ToString();
            dEditRow[5] = SiteInfDT.Rows[0][2 + 4 * 5 + 3].ToString();
            dEditRow[6] = SiteInfDT.Rows[0][2 + 4 * 6 + 3].ToString();
            dEditRow[7] = SiteInfDT.Rows[0][2 + 4 * 7 + 3].ToString();
            dEditRow[8] = SiteInfDT.Rows[0][2 + 4 * 8 + 3].ToString();
            dEditRow[9] = SiteInfDT.Rows[0][2 + 4 * 9 + 3].ToString();
            LowVolumeDT.Rows.Add(dEditRow);
            return LowVolumeDT;
        }

    }
}
