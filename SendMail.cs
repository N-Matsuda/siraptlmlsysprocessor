﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Diagnostics;

namespace SiraPtlMlSysProcessor
{
    class SendMail
    {

        public static void SendMailExec(string mlmsg, string toadr)
        {
            try
            {
                DateTime dt = DateTime.Now.ToLocalTime();
                //内容先頭
                Execute(mlmsg, toadr, dt.ToString("yyyy年MM月dd日HH時mm分")).Wait();
                Console.WriteLine("Send mail to " + toadr + mlmsg);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        public static async Task Execute(string mlmsg, string toadr, string datestr)
        {
            try
            {
                DateTime dt = DateTime.Now.ToLocalTime();
                //内容先頭
                string headwr = " 下記SSで警報発生\r\n";
                mlmsg = headwr + mlmsg + "昭和機器工業株式会社　警報自動通報メールシステム \r\n ※このメールは、送信専用メールアドレスから配信していますので、返信等は受け付けておりません。\r\n";
                string subwr = "レベビジョンクラウド警報通知メール";

                //ローカルでテスト
                var apiKey = "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";
                //サーバーデプロイ時
                //var apiKey = System.Environment.GetEnvironmentVariable("SENDGRID_APIKEY");

                var client = new SendGridClient(apiKey);

                var from = new EmailAddress("n-matsuda@skk-atgs.jp", "昭和機器工業警報自動通報メールシステム");

                var subject = subwr;

                EmailAddress to;
                //if (toadrarr[1] == "")
                to = new EmailAddress(toadr, toadr);
                //else
                //    to = new EmailAddress(toadrarr[0], toadrarr[1]);

                //mlmsg = "（" + datestr + "集信）\r\n" + mlmsg;
                var plainTextContent = mlmsg;
                mlmsg = mlmsg.Replace("\r\n", "<br>");
                var htmlContent = "<strong>" + mlmsg + "</strong>";

                var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
                //msg.AddTo(toadr, toadr);

                var response = await client.SendEmailAsync(msg).ConfigureAwait(false);

                Debug.WriteLine(response);

            }
            catch (Exception ex)
            {
                Console.WriteLine("メール転送失敗" + mlmsg);
                Console.WriteLine(ex.ToString());
            }
        }


    }
}
