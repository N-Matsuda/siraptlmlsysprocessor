﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;

namespace SiraPtlMlSysProcessor
{
    // To learn more about Microsoft Azure WebJobs SDK, please see https://go.microsoft.com/fwlink/?LinkID=320976
    class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {
            bool IsWorking = true;
            //SIRASalesData.DeleteOldData();
            //return;
            //DateTime dtn = DateTime.Now;
            //dtn = dtn.AddHours(-18);
            //DateTime stdtm = dtn.AddDays(-1);
            //CheckDeliveryData.CheckExec("M102000002", stdtm, dtn);
            //return;
#if true
#if true
            //6時間毎の各施設の在庫履歴保存処理
            List<HRegExecClass> HRegThreadPool = new List<HRegExecClass>();     //スレッドプール
            HRegExecClass HRegExec = new HRegExecClass();     //通信クラス
            HRegThreadPool.Add(HRegExec);                       //スレッドプールに作成した通信クラス登録
            Thread HRegThread1 = new Thread((new ThreadStart(HRegExec.HRegExecThread_Main)));
            HRegThread1.IsBackground = true;
            HRegThread1.Start();                              //スレッド実行開始

            while (IsWorking)                                   //全てのスレッドが終了するまで待ち合わせる
            {
                IsWorking = false;
                foreach (HRegExecClass hec in HRegThreadPool)        //各スレッドプールの動作フラグをチェック
                {
                    if (hec.IsWorking == true)
                        IsWorking = true;
                }
                Thread.Sleep(1000);
            }
#endif
            //警報メール処理
            List<ComExecClass> ThreadPool = new List<ComExecClass>(); //スレッドプール
#if true
            ComExecClass GComExec = new ComExecClass(GlobalVar.EneCompanyName, GlobalVar.EneCompanyName2, GlobalVar.CompanyEneosNo);     //通信クラス
            ThreadPool.Add(GComExec);                       //スレッドプールに作成した通信クラス登録
            Thread ComThread = new Thread((new ThreadStart(GComExec.ComThread_Main)));
            ComThread.IsBackground = true;
            ComThread.Start();                              //スレッド実行開始
#endif
#if true
            //ComExecClass GComExec2 = new ComExecClass(GlobalVar.EneCompanyName, GlobalVar.CompanyEneosNo);     //通信クラス
            ComExecClass GComExec2 = new ComExecClass(GlobalVar.JxtgCompanyName, GlobalVar.JxtgCompanyName2, GlobalVar.CompanyJxtgNo);     //通信クラス
            ThreadPool.Add(GComExec2);                       //スレッドプールに作成した通信クラス登録
            Thread ComThread2 = new Thread((new ThreadStart(GComExec2.ComThread_Main)));
            ComThread2.IsBackground = true;
            ComThread2.Start();                              //スレッド実行開始
#endif
            IsWorking = true;
            while (IsWorking)                                   //全てのスレッドが終了するまで待ち合わせる
            {
                IsWorking = false;
                foreach (ComExecClass cec in ThreadPool)        //各スレッドプールの動作フラグをチェック
                {
                    if (cec.IsWorking == true)
                        IsWorking = true;
                }
                Thread.Sleep(1000);
            }


            //SIRAデータ更新チェック
            DateTime dt = DateTime.Now.ToLocalTime();        //処理開始ログ書き込み
            DateTime dtsirachk = new DateTime(dt.Year, dt.Month, dt.Day, int.Parse(GlobalVar.SIRACHKTIME.Substring(0, 2)), int.Parse(GlobalVar.SIRACHKTIME.Substring(2, 2)), 0);
            TimeSpan tspsr = dt - dtsirachk;
            if ((tspsr.TotalMinutes < 20) && (tspsr.TotalMinutes > -20))
            {
                SiraChkClass SiraChkExec = new SiraChkClass();     //SIRAChkClass
                Thread SiraChkThread = new Thread((new ThreadStart(SiraChkExec.SiraChkThread_Main)));
                SiraChkThread.Start();                              //スレッド実行開始
                while (true)                                   //全てのスレッドが終了するまで待ち合わせる
                {
                    if (SiraChkExec.IsWorking == false)
                        break;
                    Thread.Sleep(1000);
                }
            }
            //Mobile routerデータ更新チェック
            DateTime dtmblrtchk1 = new DateTime(dt.Year, dt.Month, dt.Day, int.Parse(GlobalVar.MBLRTCHKTIME1.Substring(0, 2)), int.Parse(GlobalVar.MBLRTCHKTIME1.Substring(2, 2)), 0);
            TimeSpan tspmb1 = dt - dtmblrtchk1;
            DateTime dtmblrtchk2 = new DateTime(dt.Year, dt.Month, dt.Day, int.Parse(GlobalVar.MBLRTCHKTIME2.Substring(0, 2)), int.Parse(GlobalVar.MBLRTCHKTIME2.Substring(2, 2)), 0);
            TimeSpan tspmb2 = dt - dtmblrtchk2;
            if (((tspmb1.TotalMinutes < 20) && (tspmb1.TotalMinutes > -20)) || ((tspmb2.TotalMinutes < 20) && (tspmb2.TotalMinutes > -20)))
            {
                MblrtChkClass MblrtChkExec = new MblrtChkClass();     //SIRAChkClass
                Thread MblrtChkThread = new Thread((new ThreadStart(MblrtChkExec.MblrtChkThread_Main)));
                MblrtChkThread.Start();                              //スレッド実行開始
                while (true)                                   //全てのスレッドが終了するまで待ち合わせる
                {
                    if (MblrtChkExec.IsWorking == false)
                        break;
                    Thread.Sleep(1000);
                }
            }

            //荷卸しチェック
            dt = DateTime.Now.ToLocalTime();
            if( ( dt.Hour == 3 ) && ( dt.Minute < 30 ) )
            {
                LogData.WriteLog(dt.ToString("yyyy/MM/dd HH:mm") + " 荷卸しチェック開始");
                dt = dt.AddHours(-6);
                DateTime stdt = dt.AddDays(-1);
                SIRASite srsite = new SIRASite();
                srsite.OpenTable("*");
                int cnt = srsite.GetNumOfRecord();
                for (int i = 0; i < cnt; i++)
                {
                    MRouterTable mrt = new MRouterTable();
                    string skkcode = srsite.GetSKKCode(i);
                    mrt.OpenTableSkkcode(skkcode);
                    if (mrt.GetNumOfRecord() > 0)
                    {
                        CheckDeliveryData.CheckExec(skkcode, stdt, dt);
                    }
                }
            }
#endif
            LogData.DeleteOldLog();     //旧ログ削除
            ZHistData.DeleteOldHist();  //旧在庫履歴削除
            WtrHistory.DeleteOldWtrHist(); //旧水履歴削除
            InvData.DeleteOldData();
            if ((dt.Hour == 4) && (dt.Minute < 30))
            {
                SIRASalesData.DeleteOldData();
            }
            Console.WriteLine("Over");
        }
    }
}
