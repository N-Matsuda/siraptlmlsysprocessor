﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using System.Diagnostics;

namespace SiraPtlMlSysProcessor
{
    class CheckDeliveryData
    {
        //Constructor
        public CheckDeliveryData()
        {
            ;
        }
        public static void  CheckExec(string skkcode, DateTime strttm, DateTime endtm)
        {
#if true
            //SS毎のデータ
            SiteInfData sinf = new SiteInfData();
            int numtank = sinf.OpenTableSkkcode(skkcode);
            if (numtank == 0) //SSのデータがない場合は中断
                return;
            SiraNzlInfo snzl = new SiraNzlInfo();
            snzl.OpenTable(skkcode);
            if (snzl.GetNumOfRecord() == 0) //ノズル情報がない場合は中断
                return;
            SIRASalesData srsale = new SIRASalesData();
            srsale.OpenTableWDate(skkcode, strttm, endtm);
            if (srsale.GetNumOfRecord() == 0)　//販売履歴がない場合は中断
                return;
            //荷卸し履歴テーブル取得
            (DataTable, DataTable) dtuple = srsale.CreateSalesDT();
            DataTable DelivDT = dtuple.Item2;
            if (DelivDT == null) //荷卸し履歴がない場合は中断
                return;
            DelivDT = GetSortedDataTable(DelivDT, "タンク番号 ASC");
            int num = DelivDT.Rows.Count;
            if (num == 0)
                return;
            List<string> tnodate = new List<string>();
            for (int i = 0; i < num; i++)
            {
                try
                {
                    //タンク番号、荷卸し量、時間を取り出す
                    int tno = int.Parse(DelivDT.Rows[i][0].ToString().TrimEnd());
                    //タンク番号に連結されたタンクを取り出す
                    string lnkstr = sinf.GetTankNoStrWTankno(tno);
                    string[] tnkar = lnkstr.TrimEnd().Split(' ');
                    int lnknum = tnkar.Count();
                    string tmstr = DelivDT.Rows[i][1].ToString().TrimEnd();
                    //荷卸し時間
                    DateTime dtime = DateTime.Parse(tmstr);
                    //荷卸し量
                    string dstr = DelivDT.Rows[i][2].ToString().TrimEnd();
                    int dvol = 0;
                    int.TryParse(dstr, out dvol);
                    if (dvol == 0)
                        continue;
                    dvol = dvol / 100;
                    //同期間の在庫データテーブル取得
                    InvData invdat = new InvData();
                    DateTime sttm = dtime.AddMinutes(-60);
                    dtime = dtime.AddMinutes(10);
                    invdat.OpenTableWDate(skkcode, sttm, dtime);
                    invdat.CreateZaikoTable();
                    int inum = invdat.GetNumOfRecord();
                    //開始時の在庫を求める
                    int startvol = 0;
                    for (int j = 0; j < lnknum; j++)
                    {
                        string volstr = invdat.GetZaikoVol(int.Parse(tnkar[j]) - 1, 0);
                        int vol;
                        if (true == int.TryParse(volstr, out vol))
                            startvol += vol;
                    }
                    startvol = startvol / 100;
                    //終了時の在庫求める。
                    int endvol = 0;
                    for (int j = 0; j < lnknum; j++)
                    {
                        string volstr = invdat.GetZaikoVol(int.Parse(tnkar[j]) - 1, inum - 1);
                        int vol;
                        if (true == int.TryParse(volstr, out vol))
                            endvol += vol;
                    }
                    endvol = endvol / 100;
                    if (startvol + 1500 > endvol) //終了時に在庫が減っている場合、もしくは殆ど増加がない場合
                        continue;
                    //同期間の販売量求める
                    int salevol = 0;
                    for (int j = 0; j < lnknum; j++)
                    {
                        salevol += srsale.GetSalesVolTime(int.Parse(tnkar[j]), sttm.ToString("yyyy/MM/dd HH:mm"), dtime.ToString("yyyy/MM/dd HH:mm"));
                    }
                    //荷卸し終了時の計算在庫を求める
                    int adjvol = ((dvol + 500) / 1000) * 1000;
                    int calcvol = startvol + adjvol - salevol;
                    //実際の終了時の在庫との差を求める
                    int voldif = endvol - calcvol;
                    //dvolの値が実際の1/10の可能性があるため、1/10にして差分を比較
                    int dvol2 = dvol * 10;
                    int calcvol2 = startvol + dvol2 - salevol;
                    int voldif2 = endvol - calcvol2;
                    if (Math.Abs(voldif2) < Math.Abs(voldif))
                    {
                        dvol = dvol2;
                        calcvol = calcvol2;
                        voldif = voldif2;
                        //差分が大きい所は荷卸し量の間違いの可能性があるので修正
                        if (Math.Abs(voldif) > 800)
                        {
                            int adjvol2 = Math.Abs(voldif);
                            adjvol2 = ((adjvol2 + 500) / 1000) * 1000;
                            if (voldif < 0)
                            {
                                dvol = (dvol/ 1000) * 1000;
                                if (dvol > adjvol2)
                                    dvol -= adjvol2;
                            } else
                            {
                                dvol = (dvol/ 1000) * 1000;
                                dvol += adjvol2;
                            }
                        } else
                        {
                            dvol = (dvol / 1000) * 1000;
                        }
                        calcvol = startvol + dvol - salevol;
                        voldif = endvol - calcvol;
                    }
                    else
                    {
                        //差分が大きい所は荷卸し量の間違いの可能性があるので修正
                        if (Math.Abs(voldif) > 800)
                        {
                            int adjvol2 = Math.Abs(voldif);
                            adjvol2 = ((adjvol2 + 500) / 1000) * 1000;
                            if( voldif < 0 )
                            {
                                dvol = (dvol / 1000) * 1000;
                                if (dvol > adjvol2)
                                    dvol -= adjvol2;
                            } else
                            {
                                dvol = ((dvol+500)/ 1000) * 1000;
                                dvol += adjvol2;
                            }
                        } else
                        {
                            dvol = ( (dvol + 500) / 1000) * 1000;
                        }
                        calcvol = startvol + dvol - salevol;
                        voldif = endvol - calcvol;
                    }
                    Console.WriteLine(skkcode + " ===TANK:" + tno.ToString() + " ===DIF:" + voldif.ToString() + " ===TIME:" + dtime.ToString("yyyy/MM/dd HH:mm"));
                    Console.WriteLine(" start:" + startvol.ToString() + " delivery:" + dvol.ToString() + " sales:" + salevol.ToString());
                    Console.WriteLine(" calc:" + calcvol.ToString() + " endvol:" + endvol.ToString());
                    try
                    {
                        string sqlstr = "INSERT INTO SiraDlvChk (SKKコード, タンク番号, 日時, 開始在庫, 終了在庫, 荷卸量, 販売量, 差分) VALUES ('";
                        sqlstr += skkcode + "','" + tno.ToString() + "','" + dtime.ToString("yyyyMMddHHmm") + "','" + startvol.ToString() + "','" + endvol.ToString() + "','" + dvol.ToString() + "','" + salevol.ToString() + "','" + voldif.ToString() + "')";
                        DBCtrl.ExecNonQuery(sqlstr);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Fail to write DB");
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Fail to check delivery　" + skkcode);
                    LogData.WriteLog("Fail to check delivery　" + skkcode);
                }
            }
#else
            //SS毎のデータ
            SiteInfData sinf = new SiteInfData();
            sinf.OpenTableSkkcode(skkcode);

            SIRASalesData srsale = new SIRASalesData();
            srsale.OpenTableWDate(skkcode, strttm, endtm);
            //荷卸し履歴テーブル取得
            DataTable DelivDT = srsale.CreateSalesDT();
            DelivDT = GetSortedDataTable(DelivDT, "タンク番号 ASC");
            int num = DelivDT.Rows.Count;
            List<string> tnodate = new List<string>();
            //Tuple 型 int tno, string date, int diff, int dlvvol, int salesvol 
            var dlvres = new List<(int tno, string date, int diff, int dlvvol, int salesvol)>();
            int chktno = 0;
            if( num > 0)
                chktno = int.Parse(DelivDT.Rows[0][0].ToString().TrimEnd());
            int totaldiff = 0;
            int totaldelv = 0;
            int totalsale = 0;
            for (int i=0; i<num; i++)
            {
                try
                {
                    //タンク番号、荷卸し量、時間を取り出す
                    int tno = int.Parse(DelivDT.Rows[i][0].ToString().TrimEnd());
                    //タンク番号に連結されたタンクを取り出す
                    string lnkstr = sinf.GetTankNoStrWTankno(tno);
                    string[] tnkar = lnkstr.TrimEnd().Split(' ');
                    int lnknum = tnkar.Count();
                    string tmstr = DelivDT.Rows[i][1].ToString().TrimEnd();
                    //荷卸し時間
                    DateTime dtime = DateTime.Parse(tmstr);
                    //荷卸し量
                    //正確な荷卸し量はSIRAの販売データから持ってくる
                    int dvol = 0;
                    DateTime srdate = dtime;
                    SIRAHistoryTable srhis = new SIRAHistoryTable();
                    //2200以降の荷卸しならば翌日の日のデータを取る。
                    if (dtime.Hour >= 21)
                        srdate = dtime.AddDays(1);
                    srhis.OpenTableWDate(skkcode, int.Parse(tnkar[0]), srdate.ToString("yyyyMMdd"));
                    dvol = srhis.GetDeliveryVol();
                    if (dvol == 0)
                        continue;
  
                    //同期間の在庫データテーブル取得
                    InvData invdat = new InvData();
                    DateTime sttm = dtime.AddMinutes(-60);
                    dtime = dtime.AddMinutes(10);
                    invdat.OpenTableWDate(skkcode, sttm, dtime);
                    invdat.CreateZaikoTable();
                    int inum = invdat.GetNumOfRecord();
                    //開始時の在庫を求める
                    int startvol = 0;
                    for(int j=0; j<lnknum; j++)
                    {
                        string volstr = invdat.GetZaikoVol(int.Parse(tnkar[j])-1, 0);
                        int vol;
                        if (true == int.TryParse(volstr, out vol))
                            startvol += vol;
                    }
                    startvol = startvol/100;
                    //終了時の在庫求める。
                    int endvol = 0;
                    for (int j = 0; j < lnknum; j++)
                    {
                        string volstr = invdat.GetZaikoVol(int.Parse(tnkar[j])-1, inum-1);
                        int vol;
                        if (true == int.TryParse(volstr, out vol))
                            endvol += vol;
                    }
                    endvol = endvol / 100;
                    //同期間の販売量求める
                    int salevol = 0;
                    for (int j = 0; j < lnknum; j++)
                    {
                        salevol += srsale.GetSalesVolTime(int.Parse(tnkar[j]), sttm.ToString("yyyy/MM/dd HH:mm"), dtime.ToString("yyyy/MM/dd HH:mm"));
                    }
                    salevol = salevol / 100;
                    //実際の終了時の在庫との差を求める
                    int voldif = endvol - startvol;
                    Console.WriteLine(" tank:" + chktno.ToString() + " dif:" + voldif.ToString() + " sale:" + salevol.ToString());

                    if ( ( chktno != tno ) || ( i == num -1 ) )//タンク番号が変わった
                    {
                        if (chktno != tno)
                        {
                            totaldiff -= (totaldelv + totalsale);
                            Console.WriteLine(skkcode + " ===TANK:" + chktno.ToString() + " ===STARTTIME:" + strttm.ToString("yyyy/MM/dd") + " ===ENDTIME:" + endtm.ToString("yyyy/MM/dd"));
                            Console.WriteLine("===DIF:" + totaldiff.ToString() + " ===DELIVERY:" + totaldelv.ToString() + " === SALES:" + totalsale.ToString());
                            totaldiff = voldif;
                            totalsale = salevol;
                            totaldelv = dvol;
                            chktno = tno;
                            Console.WriteLine(" tank:" + chktno.ToString() + " totaldif:" + totaldiff.ToString() + " totalsales:" + totalsale.ToString());
                            if (i == num - 1)
                                break;
                        }
                        else
                        {
                            totaldiff += voldif;
                            totalsale += salevol;
                            totaldelv = dvol;
                            Console.WriteLine(" tank:" + chktno.ToString() + " totaldif:" + totaldiff.ToString() + " totalsales:" + totalsale.ToString());
                        }
                        if ( i == num-1 )
                        {
                            totaldiff -= (totaldelv + totalsale);
                            Console.WriteLine(skkcode + " ===TANK:" + chktno.ToString() + " ===STARTTIME:" + strttm.ToString("yyyy/MM/dd") + " ===ENDTIME:" + endtm.ToString("yyyy/MM/dd"));
                            Console.WriteLine("===DIF:" + totaldiff.ToString() + " ===DELIVERY:" + totaldelv.ToString() + " === SALES:" + totalsale.ToString());
                        }
                    }
                    else
                    {
                        totaldiff += voldif;
                        totalsale += salevol;
                        totaldelv = dvol;
                        Console.WriteLine(" totaldif:" + totaldiff.ToString() + " totalsales:" + totalsale.ToString());
                    }
                }
                catch ( Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
#endif
            Console.WriteLine("End calc");
        }
        // <summary>
        /// 指定したDataTableを対象にsort文字列で並び替えた結果を返します。
        /// </summary>
        /// <param name="dt">並び替え対象となるDataTableです。</param>
        /// <param name="sort">ソート条件</param>
        /// <returns>並び替え後のDataTalbeです。</returns>
        private static DataTable GetSortedDataTable(DataTable dt, string sort)
        {
            // dtのスキーマや制約をコピーしたDataTableを作成します。
            DataTable table = dt.Clone();

            DataRow[] rows = dt.Select(null, sort);

            foreach (DataRow row in rows)
            {
                DataRow addRow = table.NewRow();

                // カラム情報をコピーします。
                addRow.ItemArray = row.ItemArray;

                // DataTableに格納します。
                table.Rows.Add(addRow);
            }

            return table;
        }
    }
}
