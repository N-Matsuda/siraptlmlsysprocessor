﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace SiraPtlMlSysProcessor
{
    class PushNotice
    {
        protected static string[] wrg_jap = { "減警報", "満警報", "水検知警報", "センサー異常", "漏えい警報", "リーク警報", "データ更新異常" };
        protected static string[] wrg_eng = { "Low", "Full", "Water", "Sensor error", "Leak test warning", "Leak", "No data error" };
        protected static string[] pushtopic = { "LOW", "FULL", "WATER", "LEAK_TEST_WARNING", "LEAK", "SENSOR_ERROR", "NO_DATA" };
        protected static string PushnoticeURL = "https://asia-northeast1-skksmartfuelsystem.cloudfunctions.net/api/v1/sendNotification";

        private class PushMessage
        {
            public string title { get; set; }
            public string date_time_eng { get; set; }
            public string date_time_jap { get; set; }
            public string tank_no { get; set; }
            public string ss_name { get; set; }
            public string warning_name_eng { get; set; }
            public string warning_name_jap { get; set; }
        }
        private class PushData
        {
            public string ss_skkcode { get; set; }
            public string tank_name { get; set; }
        }
        private class PushPayload
        {
            public PushMessage message { get; set; }
            public PushData data { get; set; }
            public List<string> emails { get; set; }
            public string topic { get; set; }
        }
        public static void PushNoticeExec(string emailadr, string skkcode, string ssname, int wrgtype, int[] tno)
        {
            string jsonstr = "";
            try
            {
                DateTime dt = DateTime.Now.ToLocalTime();
                PushMessage pshm = new PushMessage();
                pshm.title = "Rooms";
                pshm.date_time_eng = dt.ToString("dd/MM/yyyy HH:mm");
                pshm.date_time_jap = dt.ToString("yyyy/MM/dd HH:mm");
                pshm.ss_name = ssname;
                pshm.tank_no = "No";
                pshm.warning_name_jap = wrg_jap[wrgtype];
                pshm.warning_name_eng = wrg_eng[wrgtype];
                if (wrgtype == (int)WrgType.NoDataWrg)
                {
                    pshm.tank_no = "No1";
                }
                else
                {
                    for (int i = 0; i < tno.Length; i++)
                        pshm.tank_no += tno[i] + ",";
                    pshm.tank_no = pshm.tank_no.Substring(0, pshm.tank_no.Length - 1);
                }

                PushData pshdat = new PushData();
                pshdat.ss_skkcode = skkcode;
                pshdat.tank_name = ssname;

                PushPayload pshpayld = new PushPayload();
                pshpayld.message = pshm;
                pshpayld.data = pshdat;
                List<string> emails = new List<string>();
                emails.Add(emailadr);
                pshpayld.emails = emails;
                pshpayld.topic = pushtopic[wrgtype];

                jsonstr = JsonSerializer.Serialize(pshpayld);
                //jsonstr = "{ \"message\": { \"title\": \"Rooms\",\"date_time_eng\": \"17/7/2020 12:13\",\"date_time_jap\": \"2020/7/17 12:13\",\"tank_no\": \"No1\",\"ss_name\": \"DD阪神芦屋\",\"warning_name_eng\": \"Low warning\",\"warning_name_jap\": \"低警告\" },\"data\": { \"ss_skkcode\": \"M111000001\", \"tank_name\": \"レギュラー\" },\"emails\": [\"n-matsuda@showa-kiki.co.jp\"], \"topic\": \"LOW\"}";
                //内容先頭
                PushNotification(jsonstr).Wait();
            }
            catch (Exception ex)
            {
                Console.WriteLine(skkcode + ssname + emailadr + wrgtype.ToString());
                Console.WriteLine(ex.ToString());
                //retry
                try
                {
                    PushNotification(jsonstr).Wait();
                }
                catch ( Exception ex2)
                {
                    Console.WriteLine("Retry error" + skkcode + ssname + emailadr + wrgtype.ToString());
                    Console.WriteLine(ex.ToString());
                }
            }
        }

        public static async Task PushNotification(string jsonstr)
        {

            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMilliseconds(5000);
                var content = new StringContent(jsonstr, Encoding.UTF8, "application/json");

                var response = await client.PostAsync(PushnoticeURL, content);
                Console.WriteLine(response);
            }
        }
    }
}
