﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace SiraPtlMlSysProcessor
{
    class SiraChkClass
    {
        public bool IsWorking { get; set; }
        public SiraChkClass()
        {
            IsWorking = true;
        }
        public void SiraChkThread_Main()
        {
            try
            {
                LogData.WriteLog("SIRA動作チェック開始");
                SIRASite srsite = new SIRASite();
                srsite.OpenTable("*");
                int cnt = srsite.GetNumOfRecord();
                DateTime dtnow = DateTime.Now.ToLocalTime();
                string delayss = "";
                int numdelayss = 0;
                for (int i = 0; i < cnt; i++)
                {
                    DateTime stcurdate = srsite.GetCurrentDate(i);
                    TimeSpan ts = dtnow - stcurdate;
                    string cmpcode = srsite.GetCompanyCode(i);
                    if( (ts.TotalDays >= 2) && ( cmpcode != GlobalVar.AikosekiyuCmpcode ) )
                    {
                        numdelayss += 1;
                        delayss += srsite.GetSiteName(i) + ",";
                    }
                }
                if( numdelayss > 0 )
                {
                    SiraPtlUserProfile spuser = new SiraPtlUserProfile();
                    SendMail.SendMailExec("SIRAデータ更新なしSS " + delayss, GlobalVar.MASTERMLADR);
                }
            }
            catch ( Exception ex )
            {
                Debug.WriteLine(ex.ToString());
            }
            IsWorking = false;
        }


    }
}
