﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace SiraPtlMlSysProcessor
{
    class SiraPtlWrgHis
    {
        private DataTable SPWrgHisDT;

        private const int SiteNameColNo = 0;    //施設名
        private const int SKKCodeColNo = 1;     //SKKコード
        private const int CompNameColNo = 2;    //会社名
        private const int OilTypeColNo = 3;   //液種名
        private const int TankNoColNo = 4;    //タンク番号
        private const int WrgDateColNo = 5;   //日時
        private const int WrgTypeColNo = 6;   //警報種類
        private const int TrgOrRelColNo = 7;   //0:警報解除 1:発生
        private const int WtrLvlColNo = 8;    //水位
        private const int WtrVolColNo = 9;    //水量
        private const int LC4ResColNo = 10;   //漏えい点検結果,判定値

        protected string companyname;

        //Constructer
        public SiraPtlWrgHis()
        {
            DataTableCtrl.InitializeTable(SPWrgHisDT);
        }

        //テーブル読み込み
        public void OpenTable(string cmpname)
        {
            try
            {
                companyname = cmpname;
                string sqlstr;
                if (companyname == "*")
                    sqlstr = "SELECT * FROM SiraPtlWrgHis";
                else
                    sqlstr = "SELECT * FROM SiraPtlWrgHis WHERE 会社名= (N'" + companyname + "')";
                DataTableCtrl.InitializeTable(SPWrgHisDT);
                SPWrgHisDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SPWrgHisDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //警報履歴書き込み
        public static void WriteWrgHistory(ErrDataDetail edl)
        {
            try
            {
                string sqlstr = "INSERT INTO SiraPtlWrgHis (施設名,SKKコード,会社名,液種,タンク番号,日時,警報種類,発生,水位,水量,漏えい点検結果) VALUES (N'" + edl.SiteName + "','" + edl.Skkcode + "',N'" + edl.Companyname + "','" + edl.OilTypeno.ToString() + "','" + edl.TankNo.ToString() + "','" + edl.DateStr + "','" + edl.WrgType.ToString() + "','" + edl.TrgOrRel.ToString() + "','" + edl.WtrLvl.ToString() + "','" + edl.WtrVol.ToString() + "','" + edl.LC4Res + "')";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

    }
}
