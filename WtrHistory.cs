﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiraPtlMlSysProcessor
{
    class WtrHistory
    {
        public static void DeleteOldWtrHist()
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            int hour = dt.Hour;
            if (hour < 23)  //23時以降に実施
                return;

            try
            {
                dt = dt.AddHours(-1 * GlobalVar.DataRetentionHour); //テスト 30日前
                string sqlstr = "DELETE FROM WtrHistory WHERE 日付 < '" + dt.ToString("yyyyMMddHHmm") + "'";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
